package Intermedio;

import Arbol.Temporal;
import Arbol.ArbolRutinas;
import Lexico.ArchivoSecuencial;
import Lexico.Lexema;
import Lexico.Lexico;
import Rutinas.RutinaSwitch;
import Rutinas.Rutina;
import Rutinas.RutinaCase;
import Rutinas.RutinaIf;
import Rutinas.RutinaWhile;
import Rutinas.RutinaFor;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CodigoIntermedio 
{

    public static String codInt = "";
    public String obtenerEtiquetas(String codigo) 
    {
        int etiqueta = 0;

        RutinaSwitch rutSwitch = new RutinaSwitch();
        Stack<Rutina> listaRutinas = new Stack();
        List<Integer> listaIndices = new ArrayList();
        List<Lexema> lista = (List<Lexema>) new Lexico().separa(codigo);
        for (int i = 0; i < lista.size(); i++) 
        {
            Lexema l = lista.get(i);
            if (l.getLexema().equals("SI")) 
            {
                RutinaIf rutIf = new RutinaIf("SI",(etiqueta+=10), (etiqueta+=10));
                rutIf.setEcodigo(getECodigo(lista, i+1,"¿"));
                i = i + (rutIf.getEcodigo().size()+1);
                rutIf.setScodigo(getSCodigo(lista, i));
                listaIndices.add(i+(rutIf.getScodigo().size()-1));
                listaRutinas.add(rutIf);
                codInt += rutIf.desp();
                etiqueta = rutIf.getEtiquetaActual();
                continue;
            }else if (l.getLexema().equals("MIENTRAS")) 
            {
                RutinaWhile rutWhile = new RutinaWhile("MIENTRAS",(etiqueta+=10),(etiqueta+=10), (etiqueta+=10));
                rutWhile.setEcodigo(getECodigo(lista, i+1,"¿"));
                i = i + (rutWhile.getEcodigo().size()+1);
                rutWhile.setScodigo(getSCodigo(lista, i));
                listaIndices.add(i+(rutWhile.getScodigo().size()-1));
                listaRutinas.add(rutWhile);
                codInt += rutWhile.desp();
                etiqueta = rutWhile.getEtiquetaActual();
                continue;
            }else if (l.getLexema().equals("DESDE")) 
            {
                RutinaFor rut = new RutinaFor(etiqueta+=10, etiqueta+=10, etiqueta+=10);
                
                i = i + 2;
                rut.setScodigo(getECodigo(lista, i,";"));
                
                i = i + rut.getScodigo().size();
                rut.setEcodigo(getECodigo(lista, i+1,";"));
                i = i + rut.getEcodigo().size()+2;
                List<Lexema> s2Codigo = getECodigo(lista, i,"¿");
                s2Codigo.remove(s2Codigo.size()-1);
                rut.setS2codigo(s2Codigo);
                i = i + rut.getS2codigo().size()+1;
                rut.setS3codigo(getSCodigo(lista, i));
                
                listaIndices.add(i+(rut.getS3codigo().size()));
                listaRutinas.add(rut);
                codInt += rut.desp();
                etiqueta = rut.getEtiquetaActual();
                continue;
            }else if (l.getLexema().equals("OPCION")) 
            {
                RutinaSwitch rut = new RutinaSwitch((etiqueta+=10));
                rut.setId(lista.get(i+2).getLexema());
                listaRutinas.add(rut);
                rutSwitch = rut;
                i += 4;
                continue;
            }else if (l.getLexema().equals("CASO")) 
            {
                RutinaCase rut = new RutinaCase(etiqueta+=10, etiqueta+=10, 
                        rutSwitch.getId()
                        , new Integer(lista.get(i+1).getLexema()));
                rut.setScodigo(getECodigo(lista, i+3, "ROMPE"));
                listaIndices.add(i + (rut.getScodigo().size()+1));
                listaRutinas.add(rut);
                i += 2;
                codInt += rut.desp();
                continue;
            }else if (l.getLexema().equals("DEFECTO")) 
            {
                rutSwitch.setScodigo(getECodigo(lista, i+1, "ROMPE"));
                listaIndices.add(i + (rutSwitch.getScodigo().size()+2));
                continue;
            }
            if (!l.getLexema().equals("?") || l.getLexema().equals("¿")) 
            {
                if (lista.get(i).getNoToken()>=1 && lista.get(i).getNoToken()<=4)
                {
                    if (lista.get(i).getLexema().equals("CADENA")) 
                    {
                        List<Lexema> declaracion = getECodigo(lista, i, ";");
                        codInt += "\n"+Rutina.listaToString(declaracion)+";\n";
                        i += declaracion.size()-1;
                    }
                    else if (lista.get(i+2).getLexema().equals("=")) 
                    {
                        List<Lexema> declaracion = getECodigo(lista, i, "=");
                        codInt += Rutina.listaToString(declaracion)+"\n";
                        i += declaracion.size()-1;
                        declaracion = getECodigo(lista, i, ";");
                        List<Temporal> tempDec = ArbolRutinas.generarTmp(declaracion);
                        for (Temporal t : tempDec) 
                        {
                            codInt += t.desp()+"\n";
                        }
                        i += declaracion.size()-1;
                    }else
                    {
                        List<Lexema> declaracion = getECodigo(lista, i, ";");
                        codInt += Rutina.listaToString(declaracion);
                        i += declaracion.size()-1;
                    }
                    continue;
                }
                else if (l.getTipo().equalsIgnoreCase("VARIABLE"))
                {
                    if (lista.get(i+1).getLexema().equals("=")) 
                    {
                        List<Lexema> asignacionVar = getECodigo(lista, i, ";");
                        List<Temporal> temp = ArbolRutinas.generarTmp(asignacionVar);
                        for (Temporal t : temp) 
                        {
                            codInt += "\n"+t.desp();
                        }
                        i += (asignacionVar.size());
                    }
                }else
                {
                    if(!l.getLexema().equals(";") && !l.getLexema().equals("ROMPE")
                            && !l.getTipo().equals("COMENTARIO") && !l.getTipo().startsWith("ERROR"))
                    {
                        codInt += l.getLexema()+" ";
                    }
                }
            }
            if (!listaIndices.isEmpty()) 
            {
                if (listaIndices.get(listaIndices.size()-1).equals(i)) 
                {
                    if (listaRutinas.peek() instanceof RutinaIf) 
                    {
                        codInt += "\n"+((RutinaIf)(listaRutinas.peek())).geteFalsa()+":";
                    }
                    if (listaRutinas.peek() instanceof RutinaWhile) 
                    {
                        codInt += "\ngoto "+((RutinaWhile)(listaRutinas.peek())).getInicio()+"\n";
                        codInt += "\n"+((RutinaWhile)(listaRutinas.peek())).geteFalsa()+":";
                    }
                    if (listaRutinas.peek() instanceof RutinaFor) 
                    {
                        codInt += "\n"+((RutinaFor)(listaRutinas.peek())).getExpresion();
                        codInt += "\ngoto "+((RutinaFor)(listaRutinas.peek())).getInicio()+"\n";
                        codInt += "\n"+((RutinaFor)(listaRutinas.peek())).geteFalsa()+":";
                    }
                    if (listaRutinas.peek() instanceof RutinaCase) 
                    {
                        codInt += "goto "+rutSwitch.geteSiguiente()+"\n";
                        codInt += "\n"+((RutinaCase)listaRutinas.peek()).geteFalsa()+":\n";
                    }
                    if (listaRutinas.peek() instanceof RutinaSwitch) 
                    {
                        codInt += "\n"+rutSwitch.desp();
                    }
                    listaIndices.remove(listaIndices.size()-1);
                    listaRutinas.remove(listaRutinas.size()-1);
                }
                
            }
        }
        return codInt;
    }
    
    public List<Lexema> getECodigo(List<Lexema> lista,int index, String terminaBusqueda)
    {
        List<Lexema> ecodigo = new ArrayList();
        for (int i = index; i < lista.size(); i++) 
        {
            if (lista.get(i).getLexema().trim().equals(terminaBusqueda)) 
            {
                return ecodigo;
            }
            ecodigo.add(lista.get(i));
        }
        return ecodigo;
    }
    
    public String getDeclaracion(List<Lexema> lista, int indice, String terminaAgregar)
    {
        String codIntDeclaracion = "";
        if (lista != null) 
        {
            if (indice >= 0 && indice < lista.size()) 
            {
                for (int i = indice; i < lista.size(); i++) 
                {
                    if (lista.get(indice).getLexema().equals(terminaAgregar)) 
                    {
                        return codIntDeclaracion;
                    }
                    codIntDeclaracion += lista.get(indice);
                    indice++;
                }
            }
        }
        return codIntDeclaracion;
    }
    
    public List<Lexema> getSCodigo(List<Lexema> lista,int index)
    {
        List<Lexema> scodigo = new ArrayList();
        if (lista != null) 
        {
            if (lista.get(index+1).getLexema().equals("?")) 
            {
                scodigo.add(lista.get(index));
                scodigo.add(lista.get(index+1));
                return scodigo;
            }else
            {
                int llaves = 0;
                boolean comparar = false;
            
                for (int i = index; i < lista.size(); i++) 
                {
                    if (lista.get(i).getLexema().equals("¿")) 
                    {
                        llaves++;
                        comparar = true;
                    }
                    else if (lista.get(i).getLexema().equals("?")) 
                    {
                        llaves--;
                    }
                    if( llaves == 0 && comparar)
                        return scodigo;
                    scodigo.add(lista.get(i));
                }
            }
        }
        return scodigo;
    }
    
    public boolean  buscaEnLista(List<Lexema> lista, String elementoBus)
    {
        for (Lexema elemento : lista) 
        {
            if (elemento.getLexema().equals(elementoBus)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ArchivoSecuencial arch = new ArchivoSecuencial("prueba10.txt");
        CodigoIntermedio cod = new CodigoIntermedio();
        System.out.println(cod.obtenerEtiquetas(arch.leer()));
        
        FrmCodigoIntermedio codigoIntermedio = new FrmCodigoIntermedio(codInt);
        codigoIntermedio.setVisible(true);
        codigoIntermedio.separaLineas();
    }
}
