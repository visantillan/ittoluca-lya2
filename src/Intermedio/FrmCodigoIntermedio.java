/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Intermedio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author vikto
 */
public class FrmCodigoIntermedio extends javax.swing.JFrame {

  private String codigo;
  private List<String> lineas;
  private List<String> lstnumeros = Arrays.asList("1", "2", "3");
  private DefaultTableModel model;
  private DefaultTableModel modelTemp;
  private Vector data;
  private Vector dataTemp;
  private Vector vBuscador;
  Vector vEliminador;

  private boolean cambiosBusca = false;
  private boolean cambiosElimina = false;

  /**
   * Creates new form FrmCodigoIntermedio
   */
  public FrmCodigoIntermedio() {
    initComponents();
  }

  public FrmCodigoIntermedio(String codigoIntermedio) {
    this.codigo = codigoIntermedio;
    model = new DefaultTableModel(new Object[]{"OP1", "OP2", "OP", "RESULTADO"}, 0);
    modelTemp = new DefaultTableModel();

    initComponents();
  }

  public void separaLineas() {
    String textStr[] = codigo.split("\\r\\n|\\n|\\r");

    lineas = new LinkedList(Arrays.asList(textStr));
    lineas.removeAll(Arrays.asList(null, ""));

    for (String linea : lineas) {
      if (linea.startsWith("T")) {
        String operadores[] = linea.split("\\s+");
        model.addRow(new Object[]{operadores[2], operadores[4], operadores[3],
          operadores[0]});
      }
    }

    //Obtiene toda la información de la tabla
    data = model.getDataVector();

  }

  private Vector buscador(Vector tablaRecibo) {
    Vector tabla = new Vector(tablaRecibo);
    cambiosBusca = false;
    //Iteración para búsqueda
    for (int i = 0; i < tabla.size(); i++) {
      Vector fila = (Vector) tabla.elementAt(i);
      String op1 = (String) fila.get(0);
      String op2 = (String) fila.get(1);
      String op = (String) fila.get(2);
      String t = (String) fila.get(3);

      if (NumberUtils.isNumber(op1)) {
        if (NumberUtils.isNumber(op2)) {
          Vector nuevaFila
              = operacionesBuscar(NumberUtils.toInt(op1), NumberUtils.toInt(op2), op, t);
          tabla.setElementAt(nuevaFila, i);
          cambiosBusca = true;
        } else {
          //solo op1 es número
          Vector nuevaFila = reglasBuscar(NumberUtils.toInt(op1), op, op2, t);
          if (nuevaFila.size() == 4) {
            tabla.setElementAt(nuevaFila, i);
            cambiosBusca = true;
          }
        }
      } else if (NumberUtils.isNumber(op2)) {
        Vector nuevaFila = reglasBuscar(NumberUtils.toInt(op2), op, op1, t);
        if (nuevaFila.size() == 4) {
          tabla.setElementAt(nuevaFila, i);
          cambiosBusca = true;
        }
      } else {
        //ninguno es número
      }
    }

    return tabla;
  }

  private Vector eliminador(Vector tablaRecibo) {
    Vector tabla = new Vector(tablaRecibo);
    cambiosElimina = false;
//Iteración para eliminar
    for (int i = 0; i < tabla.size(); i++) {
      Vector fila = (Vector) tabla.elementAt(i);
      Vector filaOpIgual;
      String op1 = (String) fila.get(0);
      String op2 = (String) fila.get(1);
      String op = (String) fila.get(2);
      String t = (String) fila.get(3);

      if (op.equals("=")) {
        //Recorre Columna OP1
        for (int j = 0; j < tabla.size(); j++) {
          filaOpIgual = (Vector) tabla.elementAt(j);
          if (filaOpIgual.get(0).toString().equals(t)) {
            //busca res en OP1
            Vector filaEl = new Vector();

            filaEl.add(op1);

            if (filaOpIgual.get(1).toString().equals(t)) {
              filaEl.add(op1);
            } else {
              filaEl.add(filaOpIgual.get(1));
            }

            filaEl.add(filaOpIgual.get(2));
            filaEl.add(filaOpIgual.get(3));
            tabla.setElementAt(filaEl, j);
            cambiosElimina = true;
          }
        }

        //Recorre Columna OP1
        for (int j = 0; j < tabla.size(); j++) {
          filaOpIgual = (Vector) tabla.elementAt(j);
          if (filaOpIgual.get(1).toString().equals(t)) {
            //busca res en OP1
            Vector filaEl = new Vector();

            if (filaOpIgual.get(0).toString().equals(op1)) {
              filaEl.add(op1);
            } else {
              filaEl.add(filaOpIgual.get(0));
            }
            filaEl.add(op1);
            filaEl.add(filaOpIgual.get(2));
            filaEl.add(filaOpIgual.get(3));
            tabla.setElementAt(filaEl, j);
            cambiosElimina = true;
          }
        }
      }
    }

    return tabla;
  }

  private Vector eliminaDuplicados(Vector tablaRecibo) {
    Vector tabla = new Vector(tablaRecibo);

    for (int i = 0; i < tabla.size(); i++) {
      int idx = i;
      Vector filaBusca = (Vector) tabla.get(idx);

      for (int j = 0; j < vEliminador.size(); j++) {
        if (idx != j) {
          Vector filaEl = (Vector) tabla.get(j);
          if (filaBusca.get(0).toString().equals(filaEl.get(0).toString())) {
            if (filaBusca.get(1).toString().equals(filaEl.get(1).toString())) {
              if (filaBusca.get(2).toString().equals(filaEl.get(2).toString())) {
                vEliminador.remove(j);
                tabla.remove(j);
              }
            }
          }
        }
      }
    }

    return new Vector();
  }

  private boolean sonIguales(Vector busca, Vector elimina) {

    boolean iguales = true;

    for (int i = 0; i < busca.size(); i++) {
      Vector filaBusca = (Vector) busca.get(i);
      Vector filaElimina = (Vector) elimina.get(i);
      for (int j = 0; j < filaBusca.size(); j++) {
        if (filaBusca.get(j).toString().equals(filaElimina.get(j).toString())) {
          iguales = false;
        } else {
          return true;
        }
      }
    }

    return iguales;
  }

  private void eliminaSobrantes(Vector tablaRecibe) {
    Vector tabla = new Vector(tablaRecibe);
    Vector temp = new Vector();

    for (int i = 0; i < tabla.size(); i++) {
      Vector fila = (Vector) tabla.get(i);
      if (fila.get(0).toString().equals("") || fila.get(1).toString().equals("")) {
        temp.add(fila);
      }
    }

    vEliminador.removeAll(temp);
  }

  private Vector reglasBuscar(int numero, String op, String variable, String t) {
    Vector fila = new Vector();

    int res = 0;

    switch (numero) {
      case 0:
        switch (op) {
          case "+":
            fila.add(variable);
            break;
          case "-":
            fila.add(variable);
            break;
          case "/":
            fila.add("0");
            break;
          case "*":
            fila.add("0");
            break;
          default:
            break;
        }
        break;
      case 1:
        if (op.equals("+")) {
          fila.add(variable);
        } else if (op.equals("/")) {
          fila.add(variable);
        }
        break;
      case 2:
        if (op.equals("*")) {
          fila.add(variable);
          fila.add(variable);
          fila.add("+");
        } else if (op.equals("/")) {
          fila.add(variable);
          fila.add("0.5");
          fila.add("*");
        }
        break;
      default:
        fila.add("");
        fila.add("=");
        break;
    }

    fila.add(t);

    return fila;
  }

  private Vector operacionesBuscar(int num1, int num2, String op, String t) {
    Vector fila = new Vector();
    int res = 0;

    switch (op) {
      case "+":
        res = num1 + num2;
        break;
      case "-":
        res = num1 - num2;
        break;
      case "*":
        res = num1 * num2;
        break;
      case "/":
        res = num1 / num2;
        break;
      default:
        break;
    }

    fila.add(String.valueOf(res));
    fila.add("");
    fila.add("=");
    fila.add(t);

    return fila;
  }

  /**
   * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
   * content of this method is always regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jScrollPane1 = new javax.swing.JScrollPane();
    jTable1 = new javax.swing.JTable();
    jButton1 = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Cuádruples");

    jTable1.setModel(model);
    jScrollPane1.setViewportView(jTable1);

    jButton1.setText("Optimizar");
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButton1ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 709, Short.MAX_VALUE)
          .addGroup(layout.createSequentialGroup()
            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE)))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
        .addContainerGap(19, Short.MAX_VALUE)
        .addComponent(jButton1)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap())
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    //dataTemp = model.getDataVector();
    Vector dataTemp = new Vector(model.getDataVector());

//    for (int i = 0; i < 10; i++) {
//      dataTemp = buscador(dataTemp);
//      dataTemp = eliminador(dataTemp);
//    }
    boolean continua = true;
    while (continua) {

      if (vBuscador == null) {
        vBuscador = buscador(dataTemp);
      } else {
        vBuscador = buscador(vEliminador);
      }

      vEliminador = eliminador(vBuscador);
      continua = sonIguales(vBuscador, vEliminador);
    }

    //ultimo vEliminador
    eliminaDuplicados(vEliminador);

    eliminaSobrantes(vEliminador);

    //new Object[]{"OP1", "OP2", "OP", "RESULTADO"}
    Vector columnas = new Vector();
    columnas.add("OP1");
    columnas.add("OP2");
    columnas.add("OP");
    columnas.add("RESULTADO");
    model.setDataVector(vEliminador, columnas);
  }//GEN-LAST:event_jButton1ActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(FrmCodigoIntermedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(FrmCodigoIntermedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(FrmCodigoIntermedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(FrmCodigoIntermedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new FrmCodigoIntermedio().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton jButton1;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTable jTable1;
  // End of variables declaration//GEN-END:variables
}
