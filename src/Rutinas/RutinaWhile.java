package Rutinas;

import Arbol.ArbolRutinas;
import Arbol.Temporal;
import static Rutinas.Rutina.listaToString;
import java.util.List;

public class RutinaWhile extends Rutina
{
    private int inicio;
    private int eFalsa;
    private int eVerdadera;
    private int eSiguiente;
    

    public RutinaWhile(String nombre,int inicio, int eVerdadera,int eFalsa) 
    {
        this.inicio = inicio;
        this.eFalsa = eFalsa;
        this.eVerdadera = eVerdadera;
        setNombre(nombre);
    }
    
    @Override
    public String desp()
    {
        String codigoInt = "";
        codigoInt += "\n"+inicio+":\n";
        if(buscaEnLista(getEcodigo(), "AND") || buscaEnLista(getEcodigo(), "OR"))
        {
            ArbolRutinas arbol = new ArbolRutinas(geteVerdadera(), geteFalsa());
            codigoInt += "\n"+arbol.getCodigoIntermedio(getEcodigo())+"\n";
            setEtiquetaActual(eFalsa+10);
        }else
        {
            if (getEcodigo().size()>5) 
            {
                List<Temporal> temp = ArbolRutinas.generarTmp(getEcodigo());
                String temporalStrig = temp.get(temp.size()-1).getContenido();
                for (Temporal t : temp) 
                {
                    codigoInt+= t.desp()+"\n";
                }
                codigoInt += "\nif ("+temporalStrig+") goto "+eVerdadera+"\n";
            }else
            {
                codigoInt += "\nif "+listaToString(getEcodigo())+" goto "+eVerdadera+"\n";
            }
            codigoInt += "goto "+eFalsa+"\n\n";
            codigoInt += eVerdadera+":\n";
            setEtiquetaActual(eFalsa);
        }
        return codigoInt;
    }

    @Override
    public String toString() {
        return "Nombre: "+getNombre()+" Etrue: "+eVerdadera+" EFalsa: "+eFalsa;
    }
            

    
    
    /**
     * @return the eFalsa
     */
    public int geteFalsa() {
        return eFalsa;
    }

    /**
     * @param eFalsa the eFalsa to set
     */
    public void seteFalsa(int eFalsa) {
        this.eFalsa = eFalsa;
    }

    /**
     * @return the eVerdadera
     */
    public int geteVerdadera() {
        return eVerdadera;
    }

    /**
     * @param eVerdadera the eVerdadera to set
     */
    public void seteVerdadera(int eVerdadera) {
        this.eVerdadera = eVerdadera;
    }

    /**
     * @return the eSiguiente
     */
    public int geteSiguiente() {
        return eSiguiente;
    }

    /**
     * @param eSiguiente the eSiguiente to set
     */
    public void seteSiguiente(int eSiguiente) {
        this.eSiguiente = eSiguiente;
    }

    /**
     * @return the inicio
     */
    public int getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }
}
