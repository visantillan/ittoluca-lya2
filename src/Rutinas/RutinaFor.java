package Rutinas;

import Arbol.ArbolRutinas;
import Arbol.Temporal;
import Lexico.Lexema;
import java.util.List;

public class RutinaFor extends Rutina
{
    private int inicio;
    private int eVerdadera;
    private int eFalsa;
    private int sSiguiente;
    private List<Lexema> s2codigo;
    private List<Lexema> s3codigo;

    public RutinaFor(int inicio, int eVerdadera, int eFalsta) 
    {
        this.inicio = inicio;
        this.eVerdadera = eVerdadera;
        this.eFalsa = eFalsta;
    }

    @Override
    public String desp()
    {
        String codigoInt = "";
        if(buscaEnLista(getEcodigo(), "AND") || buscaEnLista(getEcodigo(), "OR"))
        {
            ArbolRutinas arbol = new ArbolRutinas(geteVerdadera(), geteFalsa());
            codigoInt += "\n"+arbol.getCodigoIntermedio(getEcodigo());
            setEtiquetaActual(eFalsa+10);
        }else
        {
            List<Temporal> temporales = ArbolRutinas.generarTmp(getScodigo());
            for (Temporal tmp : temporales) 
            {
                codigoInt += "\n"+tmp.desp();
            }
            codigoInt += "\n\n"+inicio+":\n";
            if (getEcodigo().size()>5) 
            {
                List<Temporal> temp = ArbolRutinas.generarTmp(getEcodigo());
                String temporalStrig = temp.get(temp.size()-1).getContenido();
                for (Temporal t : temp) 
                {
                    codigoInt+= t.desp()+"\n";
                }
                codigoInt += "\nif ("+temporalStrig+") goto "+eVerdadera+"\n";
            }else
            {
                codigoInt += "\nif ("+listaToString(getEcodigo())+") goto "+eVerdadera+"\n";
            }
            codigoInt += "goto "+eFalsa+"\n\n";
            codigoInt += eVerdadera+":\n";
            setEtiquetaActual(eFalsa);
        }
        return codigoInt;
    }
    
    public String getExpresion()
    {
        String codigoExp = "";
        String nombreVar = "";
        List<Lexema> sCodigo = getScodigo();
        for (Lexema lex : sCodigo) 
        {
            if (lex.getTipo().equals("VARIABLE")) 
            {
                nombreVar = lex.getLexema();
                break;
            }
        }
        if (s2codigo.size()<3) 
        {
            if (s2codigo.get(1).getLexema().equals("++") ||
                    s2codigo.get(1).getLexema().equals("inc")) 
            {
                if (s2codigo.get(0).getTipo().equals("VARIABLE")) 
                {
                    codigoExp += nombreVar+" = "+nombreVar+" + 1 ";
                }else
                {
                    codigoExp += nombreVar+" = "+nombreVar+" + "+s2codigo.get(0);
                }
            }else
            {
                if (s2codigo.get(0).getTipo().equals("VARIABLE")) 
                {
                    codigoExp += nombreVar+" = "+nombreVar+" - 1 ";
                }else
                {
                    codigoExp += nombreVar+" = "+nombreVar+" - "+s2codigo.get(0);
                }
                
            }
        }else
        {
            String operador = s2codigo.remove(s2codigo.size()-1).getLexema();
            List<Temporal> temp = ArbolRutinas.generarTmp(s2codigo);
            Temporal ultimoTemp = temp.get(temp.size()-1);
            for (Temporal tmp : temp) 
            {
                codigoExp += tmp.desp()+"\n";
            }
            codigoExp += nombreVar+" = "+nombreVar;
            if (operador.equals("++") || operador.equals("inc"))
            {
                codigoExp += " + "+ultimoTemp.getNom();
            }else
            {
                codigoExp += " - "+ultimoTemp.getNom();
            }
            
        }
        return codigoExp;
                
    }
    
    /**
     * @return the inicio
     */
    public int getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the eVerdadera
     */
    public int geteVerdadera() {
        return eVerdadera;
    }

    /**
     * @param eVerdadera the eVerdadera to set
     */
    public void seteVerdadera(int eVerdadera) {
        this.eVerdadera = eVerdadera;
    }

    /**
     * @return the eFalsta
     */
    public int geteFalsa() {
        return eFalsa;
    }

    /**
     * @param eFalsta the eFalsta to set
     */
    public void seteFalsta(int eFalsta) {
        this.eFalsa = eFalsta;
    }

    /**
     * @return the sSiguiente
     */
    public int getsSiguiente() {
        return sSiguiente;
    }

    /**
     * @param sSiguiente the sSiguiente to set
     */
    public void setsSiguiente(int sSiguiente) {
        this.sSiguiente = sSiguiente;
    }

    /**
     * @return the s2codigo
     */
    public List<Lexema> getS2codigo() {
        return s2codigo;
    }

    /**
     * @param s2codigo the s2codigo to set
     */
    public void setS2codigo(List<Lexema> s2codigo) {
        this.s2codigo = s2codigo;
    }

    /**
     * @return the s3codigo
     */
    public List<Lexema> getS3codigo() {
        return s3codigo;
    }

    /**
     * @param s3codigo the s3codigo to set
     */
    public void setS3codigo(List<Lexema> s3codigo) {
        this.s3codigo = s3codigo;
    }
}
