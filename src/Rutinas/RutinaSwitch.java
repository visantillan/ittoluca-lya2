package Rutinas;

public class RutinaSwitch extends Rutina
{
    private int eSiguiente;
    private String id;

    public RutinaSwitch() {
    }

    
    public RutinaSwitch(int eSiguiente) 
    {
        this.eSiguiente = eSiguiente;
    }

    @Override
    public String desp() {
        String codigoInt = eSiguiente+":\n";
        return codigoInt;
    }

    
    
    /**
     * @return the eSiguiente
     */
    public int geteSiguiente() {
        return eSiguiente;
    }

    /**
     * @param eSiguiente the eSiguiente to set
     */
    public void seteSiguiente(int eSiguiente) 
    {
        this.eSiguiente = eSiguiente;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
}
