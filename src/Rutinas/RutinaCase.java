package Rutinas;

public class RutinaCase extends Rutina
{
    private int eVerdadera;
    private int eFalsa;
    private String id;
    private int noCaso;

    public RutinaCase(int eVerdadera, int eFalsa, String id, int noCaso) 
    {
        this.eVerdadera = eVerdadera;
        this.eFalsa = eFalsa;
        this.id = id;
        this.noCaso = noCaso;
    }

    
    
    @Override
    public String desp() 
    {
        String codigoInt = "";
        setEtiquetaActual(eFalsa);
        codigoInt += "\nif("+id+" = "+noCaso+") goto "+eVerdadera+"\n";
        codigoInt += "goto "+eFalsa+"\n\n";
        codigoInt += eVerdadera+":\n";
        return codigoInt;
    }

    
    
    /**
     * @return the eVerdadera
     */
    public int geteVerdadera() {
        return eVerdadera;
    }

    /**
     * @param eVerdadera the eVerdadera to set
     */
    public void seteVerdadera(int eVerdadera) {
        this.eVerdadera = eVerdadera;
    }

    /**
     * @return the eFalsa
     */
    public int geteFalsa() {
        return eFalsa;
    }

    /**
     * @param eFalsa the eFalsa to set
     */
    public void seteFalsa(int eFalsa) {
        this.eFalsa = eFalsa;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the noCaso
     */
    public int getNoCaso() {
        return noCaso;
    }

    /**
     * @param noCaso the noCaso to set
     */
    public void setNoCaso(int noCaso) {
        this.noCaso = noCaso;
    }
    
    
    
}
