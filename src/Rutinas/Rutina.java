package Rutinas;

import Lexico.Lexema;
import java.util.List;

/**
 *
 * @author ge
 */
public abstract class Rutina 
{
    private List<Lexema> ecodigo;
    private List<Lexema> scodigo;
    private String nombre;
    protected int etiquetaActual;
    

    /**
     *
     * @return
     */
    public String desp()
    {
        return "";
    }
    
    public boolean  buscaEnLista(List<Lexema> lista, String elementoBus)
    {
        for (Lexema elemento : lista) 
        {
            if (elemento.getLexema().equalsIgnoreCase(elementoBus)) {
                return true;
            }
        }
        return false;
    }
    
    public static String listaToString(List<Lexema> eCodigo)
    {
        String cadena = "";
        if (eCodigo != null) 
        {
            for (Lexema l : eCodigo) 
            {
                cadena += l.getLexema()+" ";
            }
        }
        return cadena;
    }
    
    /**
     * @return the ecodigo
     */
    public List<Lexema> getEcodigo() {
        return ecodigo;
    }

    /**
     * @param ecodigo the ecodigo to set
     */
    public void setEcodigo(List<Lexema> ecodigo) {
        this.ecodigo = ecodigo;
    }

    /**
     * @return the scodigo
     */
    public List<Lexema> getScodigo() {
        return scodigo;
    }

    /**
     * @param scodigo the scodigo to set
     */
    public void setScodigo(List<Lexema> scodigo) {
        this.scodigo = scodigo;
    }
    

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the etiquetaActual
     */
    public int getEtiquetaActual() {
        return etiquetaActual;
    }

    /**
     * @param etiquetaActual the etiquetaActual to set
     */
    public void setEtiquetaActual(int etiquetaActual) {
        this.etiquetaActual = etiquetaActual;
    }
}
