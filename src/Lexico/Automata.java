package Lexico;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//Clase de automata
public class Automata {

    /*Objetos para abrir y leer el archivo*/
    String estado;
    FileReader fr;
    BufferedReader br, brr;
    
    String nombreArch;//nombre del archivo
    String transiciones[][];//matriz de trancision
    

    /*Constructor por defecto*/
    public Automata() 
    {
        nombreArch = "";
    }
    
    //Contructor para iniciar el nombre
    public Automata(String nombreArch) 
    {
        this.nombreArch = nombreArch;
    }
    
    //Metodo para leer un archivo
    public String [][] leer() {
        try 
        {
            if (getNombreArch() != null) 
            {
                int contador = -1;
                br = new BufferedReader(new FileReader(getNombreArch()));
                while (br.readLine() != null)
                {
                    //recorremos el archivo para saber cuantas transiciones tiene
                    contador++;
                }
                br.close();
                
                brr = new BufferedReader(new FileReader(getNombreArch()));
                //creamos la matriz con sus respectivos renglones y filas
                transiciones = new String[(contador+1)][4];
                
                //con dos ciclos for vamos llenando la matriz de tranciciones
                for (int i = 0; i < transiciones.length; i++) 
                {
                    String aux[] = brr.readLine().split(" ");
                    for (int j = 0; j < transiciones[i].length; j++) 
                    {
                        transiciones[i][j] = aux[j];
                    }
                }
                brr.close();
            }
        } catch (FileNotFoundException ex)
        {
            System.out.println("Error no se encontro el archivo");
        } catch (IOException ex)
        {
            System.out.println("Error al leer el archivo");
        }
        return transiciones;
    }

    //metodo para buscar la trancisicon del automata
    public String [] busqueda(String estado, String cadena)
    {
        if (estado != null && cadena != null) 
        {
            for (int i = 0; i < transiciones.length; i++) {
                if (transiciones[i][0].equalsIgnoreCase(estado))//encontro el estado
                {   
                    if (transiciones[i][1].equalsIgnoreCase(cadena)) //encontro la transicion
                    {
                        return transiciones[i];//retorna la transicion encontrada
                    }
                } 
            }
        }
        return null;
    }
    
    /**
     * metodo para checar la cadena con el automata
     * @param cadena 
     * @return retorna la ultima transicion que realizo el algoritmoAutomata */
    public String[] algoritmoAutomata(String cadena) 
    {
        cadena = cadena.replace(" ", "");
        cadena = cadena.replace("\n", "");
        transiciones = leer();
        boolean b = true;//bandera para saber si no encontro la transicion
        String aux[] = null;
        
        if (cadena != null) 
        {
            estado = "0";
            for (int i = 0; i < cadena.length() & b; i++)
            {
                aux = busqueda(estado, cadena.charAt(i)+"");//evaluamos cada caracter 
                if (aux != null) //encontro la transicion
                {
                    estado = aux[2];
                }else
                {
                    b = false; //no encontro la transicion
                }
            }
        }
        return aux;
    }
    
    //get´s y set´s
    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }
    
//    public static void main(String[] args) {
//        
//        JFileChooser chooser  = new JFileChooser();
//        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
//        if (chooser.showOpenDialog(null)== JFileChooser.APPROVE_OPTION)
//        {
//            Automata obj = new Automata(chooser.getSelectedFile().toString());
//            Scanner scan = new Scanner(System.in);
//            System.out.println("Introduce cadena");
//            obj.algoritmoAutomata(scan.next());
//        }
//    }
}
