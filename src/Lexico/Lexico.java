package Lexico;

import java.io.File;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
import javax.swing.JFileChooser;

//clase para realizar el analisis lexico
public class Lexico extends Lexema
{

    private  String error;         //indicara que error sucecidio en el analisis
    protected Automata automata;    //objeto para llamar a los algoritmos de automatas
    protected String separadores[]; //arreglo para determinar los separadores de nuestro codigo
    
    private final String tablaTokens[][] = 
    {
        {"1","ENTERO","PALABRA RESERVADA"},
        {"2","REAL","PALABRA RESERVADA"},
        {"3","BOOLEANO","PALABRA RESERVADA"},
        {"4","CADENA","PALABRA RESERVADA"},
        {"6","+","OP. ARIT."},
        {"6","-","OP. ARIT."},
        {"6","*","OP. ARIT."},
        {"6","/","OP. ARIT."},
        {"7","--","OP. DECREM."},
        {"8","ENTONCES","PALABRA RESERVADA"},
        {"9","HACER","PALABRA RESERVADA"},
        {"10","CLASE","PALABRA RESERVADA"},
        {"11","IMPRIMIR","PALABRA RESERVADA"},
        {"12","++","OP INCREMENTO"},
        {"13","METODO","PALABRA RESERVADA"},
        {"14","PRINCIPAL","PALABRA RESERVADA"},
        {"15","NOT","PALABRA RESERVADA"},
        {"16","AND","OP. LOGICOS"},
        {"16","OR","OP. LOGICOS"},
        {"17","SI","PALABRA RESERVADA"},
        {"18","PARA","PALABRA RESERVADA"},
        {"19","MIENTRAS","PALABRA RESERVADA"},
        {"20","LEER","PALABRA RESERVADA"},
        {"21","¿","CARACTER ESPECIAL"},
        {"22","?","CARACTER ESPECIAL"},
        {"23","(","CARACTER ESPECIAL"},
        {"24",")","CARACTER ESPECIAL"},
        {"25","!","CARACTER ESPECIAL"},
        {"26",",","CARACTER ESPECIAL"},
        {"28",";","CARACTER ESPECIAL"},
        {"29","+","CARACTER ESPECIAL"},
        {"33","=","OP ASIGNACION"},
        {"37","FALSO","PALABRA RESERVADA"},
        {"38","VERDADERO","PALABRA RESERVADA"},
        {"39","OPCION","PALABRA RESERVADA"},
        {"40","CASO","PALABRA RESERVADA"},
        {"41","NUEVO","PALABRA RESERVADA"},
        {"42","[","CARACTER ESPECIAL"},
        {"43","]","CARACTER ESPECIAL"},
        {"44","DEFECTO","PALABRA RESERVADA"},
        {"45","ROMPE","PALABRA RESERVADA"},
    };

    /**constructor por defecto*/
    public Lexico() 
    {
        //*instanciamos el objeto pasando por parametro su correspondiente matriz de transiciones*/
        automata = new Automata("tabla_automata.txt");
        separadores = new String[]{" ","<=",">=","==","++","--","¿",",","?","!=",";",">","<","=","(",")","+","/","-","*","[","]",":"};
    }

    public Queue<Lexema> separaCodigo(String codigo)
    {
        Queue<Lexema> tokensAnalisados = new LinkedList();
        
        boolean termina = false;
        int posicion;
        int indiceCodigoFuente = -1;
        codigo = codigo.replace("\n", " \n");
        StringTokenizer cortaRenglones = new StringTokenizer(codigo, "\n");
        int renglones = cortaRenglones.countTokens();
        
        /*ciclo para recorrer los renglones del codigo*/
        for (int i = 0; i < renglones && !termina; i++) 
        {
            posicion = 0;   //variable para saber en que intervalo substraer una cadena
            
            String renglonCortado = cortaRenglones.nextToken();//obtiene el renglon
           
            if (renglonCortado.trim().isEmpty())//si el renglon esta vacio avanzara al siguiente
                continue;
            
            //recorrera el renglon caracter por caracter
            for (int j = 0; j < renglonCortado.length(); j++) 
            {
                //indice para recorrer cada posicion del codigo fuente
                indiceCodigoFuente++;
                
                //encontro comentario o comentario
                if (renglonCortado.charAt(j)=='\"' || renglonCortado.charAt(j)=='#') 
                {
                    //consulta el caracter que cierrra comentario o comentario en el renglon
                    int comentarioOcadena = (
                            renglonCortado.charAt(j)=='\"'
                            ?renglonCortado.indexOf('\"', j+1)
                            :renglonCortado.indexOf('#', j+1));
                    //no encontro el caracter en el renglon
                    if (comentarioOcadena<0)    
                    {
                        //buscara el caracter que cierra en todo el codigo
                        comentarioOcadena = (
                            codigo.charAt(j)=='\"'
                            ?codigo.indexOf('\"', j+1)
                            :codigo.indexOf('#', j+1));
                        
                        //no encontro caracter que cierra 
                        if (comentarioOcadena<0)
                        {
                            //agregara todo lo que falte a los tokens
                            tokensAnalisados.add(identificaLexema(codigo.substring(posicion, codigo.length()).trim(),i+1, j+1));
                            termina = true; //bandera para salirse del ciclo
                            continue; //se salta desde aqui hasta el siguiente intevalo para terminar el ciclo for
                        }else
                        {
                            /*encontro caracter que cierra en todo el codigo fuente*/
                            
                            //agregara a la lista de tokens
                            tokensAnalisados.add(
                                identificaLexema(
                                        codigo.substring(((indiceCodigoFuente-1)<0?0:indiceCodigoFuente), 
                                                comentarioOcadena+1), i+1, j+1));
                            //avanzara en renglones hasta donde se encontro caracter que cierra
                            String recorreRenglones;
                            do
                            {
                                //busca en cada renglon si esta el caracter que cierra
                                recorreRenglones = cortaRenglones.nextToken();
                                
                                comentarioOcadena = (
                                    renglonCortado.charAt(j)=='\"'
                                    ?recorreRenglones.indexOf('\"', 0)
                                    :recorreRenglones.indexOf('#', 0));
                                i++; //incrementa el numero de renglones
                            }
                            while (comentarioOcadena<0);
                            j = comentarioOcadena;
                            posicion = comentarioOcadena+1;
                            renglonCortado = recorreRenglones;
                            continue;
                        }
                            
                    }else
                    {
                        tokensAnalisados.add(
                            identificaLexema(
                                    renglonCortado.substring(((j-1)<0?0:j), 
                                            comentarioOcadena+1), i+1, j+1));
                        j = comentarioOcadena;
                        posicion = comentarioOcadena+1;
                    }
                    
                }
                for (String[] strings : tablaTokens) 
                {
                    if (renglonCortado.substring(j).startsWith(strings[1]))
                    {
                        tokensAnalisados.add(new Lexema(strings[1], new Integer(strings[0]), (i+1), (j+1), strings[2]));
                        j = j + strings[1].length();
                        posicion = j;
                        break;
                    }else
                    {
                        for (String separador : separadores) 
                        {
                            if (renglonCortado.substring(j).startsWith(separador)) 
                            {
                                String cadenaSubstraida = renglonCortado.substring(posicion, j);
                                if (!cadenaSubstraida.trim().isEmpty()) 
                                {
                                    tokensAnalisados.add(identificaLexema(cadenaSubstraida.trim(), i+1, posicion+1));
                                    if (!separador.trim().isEmpty()) 
                                    {
                                        tokensAnalisados.add(identificaLexema(separador, i+1, j+1));
                                    }
                                    posicion = j;
                                }
                            }
                        }
                    }
                }
            }
        }
        return tokensAnalisados;
    }
    
    /**
     * Metodo para separar el codigo
     * @param codigo es el codigo fuente que va a separar
     * @return retorna el codigo fuente separado en una cola de lexemas con todas sus propiedades
     */
    public Queue<Lexema> separa(String codigo)
    {
        int indiceCodigo = 0;                       //indice del codigo fuente
        int pos = 0;                                //posicion de acarreo
        linea = 0;                                  //numero de linea 
        noToken = 0;                                //numero de token de cada elemento
        boolean separador = false;                  //bandera cuando encontro separador
        boolean terminaAnalisis = false;            //bandera para cuando finalize el analisis
        codigo = codigo.replace("\n", "\n ");
        
        Lexema obj ;                                //objeto para guardar en la cola
        String nomLexema;                           //lexema de cada elemento
        Queue<Lexema> lexemas = new LinkedList();   //cola para guardar los lexemas
        
        /*instancia el objeto pasando por parametro el codigo y el delimitador*/
        StringTokenizer separa = new StringTokenizer(codigo, "\n");
        
        while (separa.hasMoreTokens() && !terminaAnalisis)  //recorremos todo el codigo mientras encuentre mas \n
        {
            linea++;
            boolean separadorCompuesto = true;          //bandera cuando encuentra un separador compuesto
            String renglon = separa.nextToken()+" ";    //renglon completo
            /*recorremos cada caracter del renglon*/
            for (int i = 0; i < renglon.length(); i++) 
            {
                for (String separadore: separadores) //recorremos todos los posibles separadores
                {
                    if ((renglon.charAt(i)+"").toString().equals("\"")) //encontro comillas
                    {
                        int comillas = renglon.indexOf("\"", i+1);  //buscamos las comillas que cierran
                        
                        if (comillas<0) //no encontro comillas que cierran
                        {
                            comillas = codigo.indexOf("\"",indiceCodigo+renglon.length());
                            if (comillas<0) 
                            {
                                lexemas.add(identificaLexema(codigo.substring(indiceCodigo+renglon.indexOf("\""), codigo.length()), linea, i+1));
                            }else
                            {
                                int inicio = indiceCodigo+i;
                                indiceCodigo = indiceCodigo+renglon.length();
                                int incrementaLinea = linea;
                                boolean caracterCierra = false;
                                int indiceRenglones = 0;
                                do 
                                {
                                    linea++;
                                    renglon = separa.nextToken();
                                    if (renglon.contains("\"")) 
                                    {
                                        pos = renglon.indexOf("\"")+1;
                                        lexemas.add(identificaLexema(codigo.substring(inicio,indiceCodigo+(pos+indiceRenglones)), incrementaLinea, i));
                                        indiceCodigo = inicio+(pos-1);
                                        caracterCierra = true;
                                        break;
                                    }
                                    indiceCodigo = indiceCodigo+renglon.length();
                                    indiceRenglones++;
                                } while (!caracterCierra);
                            }
                            i = renglon.length();
                            break;
                        }else
                        {
                            if (comillas+1 == renglon.length()) //si las comillas estan al final del renglon
                            {
                                nomLexema = renglon.subSequence(pos, comillas+1).toString();
//                                noToken = noToken(nomLexema);
//                                obj = new Lexema(nomLexema, noToken, linea, i+1,nombreToken(noToken));
                                lexemas.add(identificaLexema(nomLexema, linea, i+1));
                            }
                            i = comillas;   //nos movemos hasta el caracter de las comillas que cierran
                            break;
                        }
                    }
                    else if ((renglon.charAt(i)+"").equals("#"))    //encontro comentarios
                    {
                        int comentario = renglon.indexOf("#",i+1);  //buscamos el # que cierra el comentario
                        if (comentario<0)   //no encontro gato que cierra
                        {
//                            i = renglon.length();
//                            break;
                            if (comentario<0) //no encontro comentario que cierran
                            {
                                comentario = codigo.indexOf("#",indiceCodigo+renglon.length());
                                if (comentario<0) 
                                {
                                    lexemas.add(identificaLexema(codigo.substring(indiceCodigo+renglon.indexOf("\""), codigo.length()), linea, i+1));
                                }else
                                {
                                    int inicio = indiceCodigo+i;
                                    indiceCodigo = indiceCodigo+renglon.length();
                                    int incrementaLinea = linea;
                                    boolean caracterCierra = false;
                                    int indiceRenglones = 0;
                                    do 
                                    {
                                        linea++;
                                        renglon = separa.nextToken();
                                        if (renglon.contains("#")) 
                                        {
                                            pos = renglon.indexOf("#")+1;
                                            lexemas.add(identificaLexema(codigo.substring(inicio,indiceCodigo+(pos+indiceRenglones)), incrementaLinea, i));
                                            indiceCodigo = inicio+(pos-1);
                                            caracterCierra = true;
                                            break;
                                        }
                                        indiceCodigo = indiceCodigo+renglon.length();
                                        indiceRenglones++;
                                    } while (!caracterCierra);
                                }
                                i = renglon.length();
                                break;
                            }
                        }else
                        {
                            if (!renglon.subSequence(pos, i).toString().trim().isEmpty())   //comprobamos esto a;#a# 
                            {
                                nomLexema = renglon.subSequence(pos, i).toString(); 
//                                noToken = noToken(nomLexema);
//                                obj = new Lexema(nomLexema,noToken(nomLexema), linea, (pos+1), nombreToken(noToken(nomLexema)));
                                lexemas.add(identificaLexema(nomLexema, linea, pos +1));
                                pos = i;
                            }   
                            nomLexema = renglon.subSequence(pos, comentario+1).toString();
//                            noToken = noToken(nomLexema);
//                            obj = new Lexema(nomLexema, noToken, linea, i+1,nombreToken(noToken));
                            lexemas.add(identificaLexema(nomLexema, linea, i+1));   //insertamos en la cola el nuevo lexema
                            separador = true;   //el # funciona como un separador
                            i = comentario;     //nos movemos hasta el caracter del # que cierra
                            pos = i+1;          //movemos la posicion de acarreo al caracter siguiente del gato que cierra
                            break;
                        }
                    }
                    if ((i+1)<renglon.length()) {
                        if (renglon.subSequence(i, i+2).equals(separadore)) {
                            separador = true;
                            if (!renglon.subSequence(pos, i).toString().trim().isEmpty())   //el codigo entre separadores no es vacio
                            {
                                nomLexema = renglon.subSequence(pos, i).toString();
//                                noToken = noToken(nomLexema);
//                                obj = new Lexema(nomLexema, noToken, linea, (pos+1), nombreToken(noToken));
                                lexemas.add(identificaLexema(nomLexema, linea, pos+1));
                            }
//                            noToken = noToken(separadore);
                            nomLexema = separadore;
//                            obj = new Lexema(nomLexema, noToken, linea, (i+1), nombreToken(noToken));
                            lexemas.add(identificaLexema(nomLexema, linea, i+1));
                            separadorCompuesto = false;
                            pos = i+2;  //el acarremo se mueve
                            i = i+1;    //i se meve hasta el siguiente caracter despues del separador encontrado
                        }
                    }
                    if (separadorCompuesto) {
                        if (i<renglon.length()) {
                            /*es la misma logica que en el separador compuesto pero ahora con un caracter normal*/
                            if (renglon.subSequence(i, i+1).equals(separadore)) {
                                separador = true;
                                if (!renglon.subSequence(pos, i).toString().trim().isEmpty()) 
                                {
                                    nomLexema = renglon.subSequence(pos, i).toString();
                                    noToken = noToken(nomLexema);
                                    obj = new Lexema(nomLexema,noToken(nomLexema), linea, (pos+1), nombreToken(noToken(nomLexema)));
                                    lexemas.add(identificaLexema(nomLexema, linea, pos+1));
                                }
                                if (separadore != " ") {
                                    nomLexema = separadore;
                                    noToken = noToken(nomLexema);
                                    obj = new Lexema(nomLexema,noToken(nomLexema), linea, (i+1),nombreToken(noToken));
                                    lexemas.add(identificaLexema(nomLexema, linea, (i+1)));
                                }
                                pos = i+1;
                            }
                        }
                    }
                } //for separadores
                separadorCompuesto = true;
            }//for cadena
            if (!separador) //no encontro ningun separador
            {
                nomLexema = renglon;
                noToken = noToken(nomLexema);
                obj = new Lexema(nomLexema, noToken, linea, 1, nombreToken(noToken));
                lexemas.add(identificaLexema(nomLexema, linea, 1));
            }
            pos = 0;
            indiceCodigo = indiceCodigo+renglon.length();
        }//while
        return  lexemas;
    }
    
    public Lexema identificaLexema(String cadena, int linea, int col)
    {
        for (String[] tokens : tablaTokens) 
        {
            if (cadena.equals(tokens[1])) 
            {
                return new Lexema(cadena, new Integer(tokens[0]), linea, col, tokens[2]);
            }
        }
        Automata aut = new Automata("tabla_automata.txt");
        String trans[] = aut.algoritmoAutomata(cadena);
        if (trans != null) 
        {
            if (trans[3].equals("1"))//checamos si el automata termina en estado 1 (aceptacion)
            {
                if (aut.estado.equals("1"))//es una variable
                    return new Lexema(cadena, 5, linea, col,"VARIABLE");
                if (aut.estado.equals("2"))//es un numero entero
                    return new Lexema(cadena, 31, linea, col,"NUMERO ENTERO");
                if (aut.estado.equals("4"))//es un numero decimal o flotante
                    return new Lexema(cadena, 32, linea, col,"NUMERO REAL");
                if (aut.estado.equals("6"))//es un comentario
                    return new Lexema(cadena, 27, linea, col,"COMENTARIO");
                if (aut.estado.equals("8")) //es una cadena
                    return new Lexema(cadena, 35, linea, col,"CADENA");
                if (aut.estado.equals("11") || aut.estado.equals("10"))   //es un operador relacional
                    return new Lexema(cadena, 30, linea, col,"OP RELACIONAL");
                if (aut.estado.equals("12"))    //es un operador de asignacion
                    return new Lexema(cadena, 33, linea, col,"OP ASIGNACION");
            }
            else 
            {
                if (aut.estado.equals("0")) //error caracter desconocido
                    return new Lexema(cadena, 100, linea, col,"ERROR CARACTER DESCONOCIDO");
                if (aut.estado.equals("1")) //error caracter desconocido
                    return new Lexema(cadena, 110, linea, col,"ERROR VARIABLE INVALIDA");
                if (aut.estado.equals("2")) //error caracter desconocido
                    return new Lexema(cadena, 130, linea, col,"ERROR NUMERO INVALIDO");
                if (aut.estado.equals("3")) //error numero no valido
                    return new Lexema(cadena, 130, linea, col,"ERROR NUMERO INVALIDO");
                if (aut.estado.equals("5"))  //error comentario no valido
                    return new Lexema(cadena, 140, linea, col,"ERROR COMENTARIO INVALIDA");
                if (aut.estado.equals("7"))  //error cadena no valida
                    return new Lexema(cadena, 180, linea, col,"ERROR CADENA INVALIDA");
                if (aut.estado.equals("9"))  //error operador relacional no valido
                    return new Lexema(cadena, 150, linea, col,"ERROR OP RELACIONAL INVALIDO ");
            }
        } else 
        {
            if (aut.estado.equals("0")) //error caracter desconocido
                return new Lexema(cadena, 100, linea, col,"ERROR CARACTER DESCONOCIDO");
            if (aut.estado.equals("1")) //error caracter desconocido
                return new Lexema(cadena, 110, linea, col,"ERROR VARIABLE INVALIDA");
            if (aut.estado.equals("2")) //error caracter desconocido
                    return new Lexema(cadena, 130, linea, col,"ERROR NUMERO INVALIDO");
            if (aut.estado.equals("3")) //error numero no valido
                return new Lexema(cadena, 130, linea, col,"ERROR NUMERO INVALIDO");
            if (aut.estado.equals("5"))  //error comentario no valido
                return new Lexema(cadena, 140, linea, col,"ERROR COMENTARIO INVALIDA");
            if (aut.estado.equals("7"))  //error cadena no valida
                return new Lexema(cadena, 180, linea, col,"ERROR CADENA INVALIDA");
            if (aut.estado.equals("9"))  //error operador relacional no valido
                return new Lexema(cadena, 150, linea, col,"ERROR OP RELACIONAL INVALIDO ");
        }
        return null;
    }
    /**
     * Metodo para saber que tipo de token es el lexema
     * @param codigo es la cadena que va a determinar su numero de token
     * @return retorna el numero de token del lexema
     */
    public int noToken(String codigo) 
    {
        Automata aut = new Automata("tabla_automata.txt");
        codigo = codigo.replace(" ", "");

        noToken = 0;

        boolean banderaToken = true;//nos ayudara a saber si es un token fijo o un automata

        /*checamos con todos los posibles tokens que tenemos*/
        if (codigo.equals("ENTERO")) {
            noToken = 1;
            banderaToken = false;
        }
        if (codigo.equals("DECIMAL")) {
            noToken = 2;
            banderaToken = false;
        }
        if (codigo.equals("FLOTANTE")) {
            noToken = 3;
            banderaToken = false;
        }
        if (codigo.equals("CADENA")) {
            noToken = 4;
            banderaToken = false;
        }
        if (codigo.equals("LOGICO")) {
            noToken = 39;
            banderaToken = false;
        }
        if (codigo.equals("+") || codigo.equals("-")
                || codigo.equals("/") || codigo.equals("*")) {
            noToken = 6;
            banderaToken = false;
        }
        if (codigo.equals("--")) {
            noToken = 7;
            banderaToken = false;
        }
        if (codigo.equals("ENTONCES")) {
            noToken = 8;
            banderaToken = false;
        }
        if (codigo.equals("HACER")) {
            noToken = 9;
            banderaToken = false;
        }
        if (codigo.equals("CLASE")) {
            noToken = 10;
            banderaToken = false;
        }
        if (codigo.equals("IMPRIMIR")) {
            noToken = 11;
            banderaToken = false;
        }
        if (codigo.equals("++")) {
            noToken = 12;
            banderaToken = false;
        }
        if (codigo.equals("METODO")) {
            noToken = 13;
            banderaToken = false;
        }
        if (codigo.equals("PRINCIPAL")) {
            noToken = 14;
            banderaToken = false;
        }
        if (codigo.equals("NOT")) {
            noToken = 15;
            banderaToken = false;
        }
        if (codigo.equals("AND") || codigo.equals("OR")) {
            noToken = 16;
            banderaToken = false;
        }
        if (codigo.equals("SI")) {
            noToken = 17;
            banderaToken = false;
        }
        if (codigo.equals("PARA")) {
            noToken = 18;
            banderaToken = false;
        }
        if (codigo.equals("MIENTRAS")) {
            noToken = 19;
            banderaToken = false;
        }
        if (codigo.equals("LEER")) {
            noToken = 20;
            banderaToken = false;
        }
        if (codigo.equals("¿")) {//separador
            noToken = 21;
            banderaToken = false;
        }
        if (codigo.equals("?")) {//separador
            noToken = 22;
            banderaToken = false;
        }
        if (codigo.equals("(")) {//separador
            noToken = 23;
            banderaToken = false;
        }
        if (codigo.equals(")")) {//separador
            noToken = 24;
            banderaToken = false;
        }
        if (codigo.equals("!")) {//separador
            noToken = 25;
            banderaToken = false;
        }
        if (codigo.equals(",")) {//separador
            noToken = 26;
            banderaToken = false;
        }
        if (codigo.equals(";")) {//separador
            noToken = 28;
            banderaToken = false;
        }
        if (codigo.equals("+")) {//separador
            noToken = 29;
            banderaToken = false;
        }
        if (codigo.equals("FALSO")) {
            noToken = 37;
            banderaToken = false;
        }
        if (codigo.equals("VERDADERO")) {
            noToken = 38;
            banderaToken = false;
        }
        if (codigo.equals("GLOBAL")) {
            noToken = 40;
            banderaToken = false;
        }
        if (banderaToken) {
            //llamamos al metodo que realiza el automata correspondiente
            String trans[] = aut.algoritmoAutomata(codigo);
            if (trans != null) {
                if (trans[3].equals("1"))//checamos si el automata termina en estado 1 (aceptacion)
                {
                    if (aut.estado.equals("1")) { //es una variable
                        noToken = 5;
                    }
                    if (aut.estado.equals("2")) { //es un numero entero
                        noToken = 31;
                    }
                    if (aut.estado.equals("4")) { //es un numero decimal o flotante
                        noToken = 32;
                    }
                    if (aut.estado.equals("6")) { //es un comentario
                        noToken = 27;
                    }
                    if (aut.estado.equals("8")) { //es una cadena
                        noToken = 35;
                    }
                    if (aut.estado.equals("11") || aut.estado.equals("10")) {   //es un operador relacional
                        noToken = 30;
                    }
                    if (aut.estado.equals("12")) {    //es un operador de asignacion
                        noToken = 33;
                    }
                } else 
                {
                    if (aut.estado.equals("0")) { //error caracter desconocido
                        noToken = 100;
                    }
                    if (aut.estado.equals("3")) { //error numero no valido
                        noToken = 130;
                    }
                    if (aut.estado.equals("5")) { //error comentario no valido
                        noToken = 140;
                    }
                    if (aut.estado.equals("7")) { //error cadena no valida
                        noToken = 180;
                    }
                    if (aut.estado.equals("9")) { //error operador relacional no valido
                        noToken = 150;
                    }
                }
            } else {
                noToken = 170;   //error desconocido
            }
        }
        return noToken;
    }
    
    /**
     * metodo para saber el nombre del token
     * @param noToken numero de token del lexema
     * @return tokenPalabra el nombre del respectivo token
     */
    public String nombreToken(int noToken)
    {
        String tokenPalabra;
        switch (noToken) 
        {
            case 1:tokenPalabra = "PALABRA RESERVADA ENTERO";break;
            case 2:tokenPalabra = "PALABRA RESERVADA DECIMAL";break;
            case 3:tokenPalabra = "PALABRA RESERVADA FLOTANTE";break;
            case 4:tokenPalabra = "PALABRA RESERVADA CADENA";break;
            case 5:tokenPalabra = "VARIABLE";break;
            case 6:tokenPalabra = "OP ARITMETICO";break;
            case 7:tokenPalabra = "OP DECREMENTO";break;
            case 8:tokenPalabra = "PALABRA RESERVADA";break;
            case 9:tokenPalabra = "PALABRA RESERVADA";break;
            case 10:tokenPalabra = "PALABRA RESERVADA";break;
            case 11:tokenPalabra = "PALABRA RESERVADA";break;
            case 12:tokenPalabra = "OP INCREMENTO";break;
            case 13:tokenPalabra = "PALABRA RESERVADA";break;
            case 14:tokenPalabra = "PALABRA RESERVADA";break;
            case 16:tokenPalabra = "OP LOGICO";break;
            case 17:tokenPalabra = "PALABRA RESERVADA";break;
            case 18:tokenPalabra = "PALABRA RESERVADA";break;
            case 19:tokenPalabra = "PALABRA RESERVADA";break;
            case 20:tokenPalabra = "PALABRA RESERVADA";break;
            case 21:tokenPalabra = "CARACTER ESPECIAL";break;
            case 22:tokenPalabra = "CARACTER ESPECIAL";break;
            case 23:tokenPalabra = "CARACTER ESPECIAL";break;
            case 24:tokenPalabra = "CARACTER ESPECIAL";break;
            case 26:tokenPalabra = "CARACTER ESPECIAL";break;
            case 27:tokenPalabra = "COMENTARIO";break;
            case 28:tokenPalabra = "CARACTER ESPECIAL";break;
            case 29:tokenPalabra = "OP. ARITMETICO";break;
            case 30:tokenPalabra = "OP RELACIONAL";break;
            case 31:tokenPalabra = "NUMERO ENTERO";break;
            case 32:tokenPalabra = "NUMERO DECIMAL";break;
            case 33:tokenPalabra = "OP ASIGNACION";break;
            case 34:tokenPalabra = "TIPO DATO";break;
            case 35:tokenPalabra = "OBJETO CADENA";break;
            case 36:tokenPalabra = "NUMEROS";break;
            case 37:tokenPalabra = "PALABRA RESERVADA";break;
            case 38:tokenPalabra = "PALABRA RESERVADA";break;
            case 39:tokenPalabra = "PALABRA RESERVADA LOGICO";break;
            case 100:tokenPalabra = "ERROR CARACTER NO VALIDO";break;
            case 130:tokenPalabra = "ERROR NUMERO NO VALIDO";break;
            case 140:tokenPalabra = "ERROR COMENTARIO NO VALIDO";break;
            case 180:tokenPalabra = "ERROR CADENA NO VALIDA";break;
            case 150:tokenPalabra = "ERROR OPERADOR RELACIONAL NO VALIDO";break;
            case 170:tokenPalabra = "ERROR FATAL";break;
                default: tokenPalabra = "ERROR DESCONOCIDO";
        }
        return tokenPalabra;
    }
    
    /**
     * metodo para checa el codigo fuente
     *
     * @return retorna si el analisis es correpto o no
     */
    public boolean  analisarLexico(String codigo)
    {
        boolean bandera = true;
        Object arreglo [] = (Object[]) separa(codigo).toArray();
        for (Object arreglo1 : arreglo) {
            Lexema obj = (Lexema) arreglo1;
            if (obj.getTipo() == "ERROR") 
                bandera = false;
//            System.out.println(obj.desp());
        }
        return bandera;
    }

    /**
     * Retorna el error del analisis
     *
     * @return error que ocurrio
     */
    public String getError()
    {
        return this.error;
    }
    
    public static void main(String[] args) 
    {
        JFileChooser chooser  = new JFileChooser();
        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        if (chooser.showOpenDialog(null)== JFileChooser.APPROVE_OPTION)
        {
            ArchivoSecuencial arch = new ArchivoSecuencial(chooser.getSelectedFile().toString());
            Lexico lexico = new Lexico();
            Queue<Lexema> result = lexico.separa(arch.leer());
            for (Lexema lexema : result) 
            {
                System.out.println(lexema.desp());
            }
            
        }
    }
}