package Lexico;

/*clase para manejar objetos con las propiedades de un lexema*/
public class Lexema {

    protected String lexema;
    protected int noToken;
    protected int linea;
    protected int columna;
    protected String tipo;

    /*constructor por defecto*/
    public Lexema() {}

    /*constructor para asignarle valores a las variables*/
    public Lexema(String lexema, int noToken, int linea, int columna, String tipo) 
    {
        this.lexema = lexema;
        this.noToken = noToken;
        this.linea = linea;
        this.columna = columna;
        this.tipo = tipo;
    }

    public String desp()
    {
        return lexema+"\t"
                + "No. Token:"+noToken+" "
                + "Linea:"+linea+" "
                + "Columna:"+getColumna()+" "
                + "Tipo:"+getTipo();
    }

    public String toString() 
    {
        return lexema;
    }

    public String getLexema() {
        return lexema;
    }

    public int getLinea() {
        return linea;
    }

    public int getNoToken() {
        return noToken;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public void setNoToken(int noToken) {
        this.noToken = noToken;
    }
    
    public void setColumna(int columna) {
        this.columna = columna;
    }

    public int getColumna() {
        return columna;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
