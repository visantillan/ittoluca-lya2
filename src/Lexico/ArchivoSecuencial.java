package Lexico;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JFileChooser;

public class ArchivoSecuencial 
{
    BufferedReader br;//objeto para leer el archivo
    FileReader fr;//objeto para establecer el archivo a leer
    FileWriter fw;//objeto para escribir el archivo
    String nombreArch;//nombre del archivo
    StringBuffer flujo;//objeto para almacenar los datos del archivo

    //Contructor por defecto
    public ArchivoSecuencial() 
    {
        nombreArch = "";
    }
    
    //Contructor para iniciar el nombre
    public ArchivoSecuencial(String nombreArch) 
    {
        this.nombreArch = nombreArch;
    }

    //Metodo para leer el archivo
    public String leer()
    {
        if (getNombreArch()!=null) 
        {
            flujo = new StringBuffer();
            String texto;
            try 
            {
                //instanciamos el buffer con su respectivo archivo
                br = new BufferedReader(new FileReader(getNombreArch()));
                
                //recorremos(leemos) todo el archivo
                while ((texto = br.readLine()) != null) 
                {        
                    flujo.append(texto).append("\n");//anexamos al flujo mientras va leyendo
                }
            } catch (IOException ex) {
                System.out.println("No se pudo leer archivo");
            }
        }
        return flujo.toString();//retornamos el flujo convertido a cadena
    }
    
    //Metodo para escribir en el archivo
    public void escribir(String cadena)
    {
            PrintWriter pw;//objeto para escribir en el archivo
            try {
                //instanciamos el FileWriter con su respectivo archivo
                fw = new FileWriter(getNombreArch());
                
                //instanciamos el PrintWriter con el respectivo FileWriter
                pw = new PrintWriter(fw);
                pw.println(cadena);//escribimos la cadena en el archivo
                pw.close();//cerramos el archivo
            } catch (IOException ex) {
                System.out.println("No se pudo escribir en el archivo");
            }
    }
    
    //GET´S Y SET'S
    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }
    
}
