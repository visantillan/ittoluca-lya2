package Arbol;

import Lexico.Lexema;
import Semantico.Posfijo;
import Semantico.PosfijoMoni;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ArbolRutinas {

  ArbolEtiquetas arbolEtiquetas = new ArbolEtiquetas();
  public static int noTemporal = 0;
  private int eVerdadera;
  private int eFalsa;
  String codigoInt = "";
  public int etiqueta;
  private Nodo raiz;
  Etiquetas e1;
  Etiquetas e2;

  public ArbolRutinas() {
  }

  public ArbolRutinas(int eVerdadera, int eFalsa) {
    this.eVerdadera = eVerdadera;
    this.eFalsa = eFalsa;
    this.etiqueta = eFalsa;
  }

  public void generaEtiquetas(Nodo raiz, Nodo raizEtiquetas) {
    if (raiz != null) {
      Lexema obj = (Lexema) raiz.getObj();
      if (obj.getLexema().equalsIgnoreCase("AND")) {
        Lexema izq = (Lexema) raiz.getIzq().getObj();
        Lexema der = (Lexema) raiz.getDer().getObj();

        if (arbolEtiquetas.getRaiz() == null) {
          Nodo r = new Nodo(new Etiquetas("AND"));
          r.setLado("r");
          Etiquetas etiquetaIzq = new Etiquetas(etiqueta += 10, eFalsa, izq.getLexema());
          etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n\n" + etiquetaIzq.geteVerdadera() + ":");
          Nodo ni = new Nodo(etiquetaIzq);
          ni.setLado("i");
          Etiquetas etiquetaDer = new Etiquetas(eVerdadera, eFalsa, der.getLexema());
          etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n\n" + etiquetaDer.geteVerdadera() + ":");
          Nodo nd = new Nodo(etiquetaDer);
          nd.setLado("d");
          r.setIzq(ni);
          r.setDer(nd);
          e1 = (Etiquetas) ni.getObj();
          e2 = (Etiquetas) nd.getObj();
          raizEtiquetas = r;
          arbolEtiquetas.setRaiz(raizEtiquetas);
        } else {
          if (raiz.getLado().equals("d")) {
            Nodo r = new Nodo(new Etiquetas("AND"));
            r.setLado("r");
            Etiquetas etiquetaIzq = new Etiquetas(etiqueta += 10, e2.geteFalsa(), izq.getLexema());
            etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n\n" + etiquetaIzq.geteVerdadera() + ":");
            Nodo ni = new Nodo(etiquetaIzq);
            ni.setLado("i");
            Etiquetas etiquetaDer = new Etiquetas(e2.geteVerdadera(), e2.geteFalsa(), der.getLexema());
            etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n\n" + etiquetaDer.geteVerdadera() + ":");
            Nodo nd = new Nodo(etiquetaDer);
            nd.setLado("d");
            e1 = (Etiquetas) ni.getObj();
            e2 = (Etiquetas) nd.getObj();
            r.setIzq(ni);
            r.setDer(nd);
            raizEtiquetas.setDer(r);
          } else if (raiz.getLado().equals("i")) {
            Nodo r = new Nodo(new Etiquetas("AND"));
            r.setLado("r");
            Etiquetas etiquetaIzq = new Etiquetas(etiqueta += 10, e1.geteFalsa(), izq.getLexema());
            etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n\n" + etiquetaIzq.geteVerdadera() + ":");
            Nodo ni = new Nodo(etiquetaIzq);
            ni.setLado("i");
            Etiquetas etiquetaDer = new Etiquetas(e1.geteVerdadera(), e1.geteFalsa(), der.getLexema());
            etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n\n" + etiquetaDer.geteVerdadera() + ":");
            Nodo nd = new Nodo(etiquetaDer);
            nd.setLado("d");
            r.setIzq(ni);
            r.setDer(nd);
            e1 = (Etiquetas) ni.getObj();
            e2 = (Etiquetas) nd.getObj();
            raizEtiquetas.setIzq(r);
          }
        }
      } else if (obj.getLexema().equalsIgnoreCase("OR")) {
        Lexema izq = (Lexema) raiz.getIzq().getObj();
        Lexema der = (Lexema) raiz.getDer().getObj();

        if (arbolEtiquetas.getRaiz() == null) {
          Nodo r = new Nodo(new Etiquetas("OR"));
          r.setLado("r");
          Etiquetas etiquetaIzq = new Etiquetas(eVerdadera, etiqueta += 10, izq.getLexema());
          etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n\n" + etiquetaIzq.geteFalsa());
          Nodo ni = new Nodo(etiquetaIzq);
          ni.setLado("i");
          Etiquetas etiquetaDer = new Etiquetas(eVerdadera, eFalsa, der.getLexema());
          etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n\n" + etiquetaDer.geteVerdadera());
          Nodo nd = new Nodo(etiquetaDer);
          nd.setLado("d");
          e1 = (Etiquetas) ni.getObj();
          e2 = (Etiquetas) nd.getObj();
          r.setIzq(ni);
          r.setDer(nd);
          raizEtiquetas = r;
          arbolEtiquetas.setRaiz(raizEtiquetas);
        } else {
          if (raiz.getLado().equals("d")) {
            Nodo r = new Nodo(new Etiquetas("OR"));
            r.setLado("r");
            Etiquetas etiquetaIzq = new Etiquetas(e2.geteVerdadera(), etiqueta += 10, izq.getLexema());
            etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n" + etiquetaIzq.geteFalsa() + ":");
            Nodo ni = new Nodo(etiquetaIzq);
            ni.setLado("i");
            Etiquetas etiquetaDer = new Etiquetas(e2.geteVerdadera(), e2.geteFalsa(), der.getLexema());
            etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n" + etiquetaDer.geteVerdadera() + ":");
            Nodo nd = new Nodo(etiquetaDer);
            nd.setLado("d");
            e1 = (Etiquetas) ni.getObj();
            e2 = (Etiquetas) nd.getObj();
            r.setIzq(ni);
            r.setDer(nd);
            raizEtiquetas.setDer(r);
          } else if (raiz.getLado().equals("i")) {
            Nodo r = new Nodo(new Etiquetas("OR"));
            r.setLado("r");
            Etiquetas etiquetaIzq = new Etiquetas(e1.geteVerdadera(), etiqueta += 10, izq.getLexema());
            etiquetaIzq.setCodigo("goto " + etiquetaIzq.geteVerdadera() + "\ngoto " + etiquetaIzq.geteFalsa() + "\n" + etiquetaIzq.geteFalsa() + ":");
            Nodo ni = new Nodo(etiquetaIzq);
            ni.setLado("i");
            Etiquetas etiquetaDer = new Etiquetas(e1.geteVerdadera(), e1.geteFalsa(), der.getLexema());
            etiquetaDer.setCodigo("goto " + etiquetaDer.geteVerdadera() + "\ngoto " + etiquetaDer.geteFalsa() + "\n" + etiquetaDer.geteVerdadera() + ":");
            Nodo nd = new Nodo(etiquetaDer);
            nd.setLado("d");
            e1 = (Etiquetas) ni.getObj();
            e2 = (Etiquetas) nd.getObj();
            r.setIzq(ni);
            r.setDer(nd);
            raizEtiquetas.setIzq(r);
          }
        }
      }
      if (raizEtiquetas.getIzq() != null) {
        generaEtiquetas(raiz.getIzq(), raizEtiquetas.getIzq());
      } else {
        generaEtiquetas(raiz.getIzq(), raizEtiquetas);
      }

      if (!raiz.equals(null)) {
        if (raiz.getIzq() != null && raizEtiquetas.getIzq() != null) {
          e1 = (Etiquetas) raizEtiquetas.getIzq().getObj();
        }
        if (raiz.getDer() != null && raizEtiquetas.getDer() != null) {
          e2 = (Etiquetas) raizEtiquetas.getDer().getObj();
        }
      }
      if (raizEtiquetas.getDer() != null) {
        generaEtiquetas(raiz.getDer(), raizEtiquetas.getDer());
      } else {
        generaEtiquetas(raiz.getDer(), raizEtiquetas);
      }

    }
  }

  /**
   *
   * @param listaNueva
   * @return Primer versión del método
   */
  public static List<Temporal> generarTmp(List<Lexema> listaNueva) {
    int prioridad;      //variable de ayuda para saber que prioridad tiene un operador

    Posfijo pos = new Posfijo();  //objeto para que llama al metodo posfijo
    Stack<Temporal> temp = new Stack(); //pila para guardar todos los temporales que generemos

    Object arreglo[] = pos.posfijo(listaNueva.toArray());
    List<Object> lista = new ArrayList();
    for (int i = 0; i < arreglo.length; i++) {
      lista.add((Lexema) arreglo[i]);
    }
    Object aux;
    for (int i = 0; i < lista.size(); i++) {
      Temporal obj = new Temporal();
      aux = lista.get(i);
      prioridad = pos.prioridad(aux.toString());  //optenemos la prioridad del elemento
      if (prioridad >= 0 && prioridad <= 4) //es un operador
      {
        if (prioridad == 0) // si es un =
        {
          obj.setNom(lista.get(0).toString());
          obj.setOp("");
          obj.setIzq(lista.get(i - 1));
          obj.setDer("");
          lista.remove(i - 2);  //removemos de la lista un dato
          lista.remove(i - 2);  //removemos de la lista un segundo dato

          //cambiamos el operador que estaba por el temporal que acabamos de crear
          lista.set(i - 2, obj);
        } else {
          noTemporal++;
          //establecemos los valores del Temporal
          obj.setNom("T" + noTemporal);
          obj.setOp(aux.toString());
          obj.setIzq(lista.remove(i - 2));  //removemos de la lista un dato
          obj.setDer(lista.remove(i - 2));  //removemos de la lista un segundo dato

          //cambiamos el operador que estaba por el temporal que acabamos de crear
          lista.set(i - 2, obj);
        }
        temp.add(obj);
        i = i - 2;
      }
    }
    return temp;
  }

  public void generaArbolRutinas(List<Lexema> listaNueva) {
    int prioridad;      //variable de ayuda para saber que prioridad tiene un operador
    Posfijo pos = new Posfijo();  //objeto para que llama al metodo posfijo

    Object arreglo[] = pos.posfijo(listaNueva.toArray());
    List<Object> lista = new ArrayList();

    for (int i = 0; i < arreglo.length; i++) {
      lista.add((Lexema) arreglo[i]);
    }

    Object aux;
    for (int i = 0; i < lista.size(); i++) {
      aux = lista.get(i);
      prioridad = pos.prioridad(aux.toString());  //optenemos la prioridad del elemento
      if (prioridad >= 2 && prioridad <= 4) {
        Lexema lex1 = (Lexema) lista.remove(i - 2);
        Lexema lex2 = (Lexema) lista.remove(i - 2);
        ((Lexema) aux).setLexema(lex1 + " " + aux + " " + lex2);
        lista.set(i - 2, aux);
        i = i - 2;
      } else if (prioridad == 1) //es un operador
      {
        Nodo nr = new Nodo(lista.get(i));
        Nodo ni, nd;
        nr.setLado("r");
        if (lista.get(i - 2) instanceof Nodo) {
          ni = (Nodo) lista.remove(i - 2);
          ni.setLado("i");
        } else {
          ni = new Nodo(lista.remove(i - 2));
          ni.setLado("i");
        }
        if (lista.get(i - 2) instanceof Nodo) {
          nd = (Nodo) lista.remove(i - 2);
          nd.setLado("d");
        } else {
          nd = new Nodo(lista.remove(i - 2));
          nd.setLado("d");
        }
        nr.setIzq(ni);
        nr.setDer(nd);
        raiz = nr;
        lista.set(i - 2, raiz);
        i = i - 2;
      }
    }
  }

//    public Temporal buscaTemporal(List<Temporal> lista, String nombreTmp)
//    {
//        if (lista != null) 
//        {
//            for (Temporal tmp : lista) 
//            {
//                if (tmp.getNom().equalsIgnoreCase(nombreTmp)) 
//                {
//                    return tmp;
//                }
//            }
//        }
//        return null;
//    }
  public String getCodigoIntermedio(List<Lexema> eCodigo) {
    generaArbolRutinas(eCodigo);
    generaEtiquetas(getRaiz(), getArbolEtiquetas().getRaiz());
    getArbolEtiquetas().infijoRutinaLogica(getArbolEtiquetas().getRaiz());
    return getArbolEtiquetas().getCodigoInter();
  }

  /**
   * @return the raiz
   */
  public Nodo getRaiz() {
    return raiz;
  }

  /**
   * @param raiz the raiz to set
   */
  public void setRaiz(Nodo raiz) {
    this.raiz = raiz;
  }

  public ArbolEtiquetas getArbolEtiquetas() {
    return arbolEtiquetas;
  }

  public void setArbol2(ArbolEtiquetas arbol) {
    arbolEtiquetas = arbol;
  }

  /**
   * @return the eVerdadera
   */
  public int geteVerdadera() {
    return eVerdadera;
  }

  /**
   * @param eVerdadera the eVerdadera to set
   */
  public void seteVerdadera(int eVerdadera) {
    this.eVerdadera = eVerdadera;
  }

  /**
   * @return the eFalsa
   */
  public int geteFalsa() {
    return eFalsa;
  }

  /**
   * @param eFalsa the eFalsa to set
   */
  public void seteFalsa(int eFalsa) {
    this.eFalsa = eFalsa;
  }
}
