package Arbol;

public class Nodo 
{
    private String lado;
    private Nodo izq;
    private Nodo der;
    private Object obj;

    public Nodo(Object obj) {
        this.obj = obj;
    }

    public String desp()
    {
        String cadena = "";
        return cadena;
    }
    /**
     * @return the izq
     */
    public Nodo getIzq() {
        return izq;
    }

    /**
     * @param izq the izq to set
     */
    public void setIzq(Nodo izq) {
        this.izq = izq;
    }

    /**
     * @return the der
     */
    public Nodo getDer() {
        return der;
    }

    /**
     * @param der the der to set
     */
    public void setDer(Nodo der) {
        this.der = der;
    }

    /**
     * @return the obj
     */
    public Object getObj() {
        return obj;
    }

    /**
     * @param obj the obj to set
     */
    public void setObj(Object obj) {
        this.obj = obj;
    }

    /**
     * @return the lado
     */
    public String getLado() {
        return lado;
    }

    /**
     * @param lado the lado to set
     */
    public void setLado(String lado) {
        this.lado = lado;
    }
}
