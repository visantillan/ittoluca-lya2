package Arbol;

public class Temporal 
{
    private String nom;
    private Object izq;
    private Object der;
    private Object op;
    private Temporal tmp;

    public Temporal() {}

    public Temporal(String nom, Object izq, Object der, Object op, Temporal tmp) 
    {
        this.nom = nom;
        this.izq = izq;
        this.der = der;
        this.op = op;
        this.tmp = tmp;
    }
    
    public String desp()
    {
        String codigo = "";
        codigo += nom+" = ";
        if (izq instanceof Temporal) 
        {
            codigo += ((Temporal)izq).getNom()+" ";
        }else
        {
            codigo += izq+" ";
        }
        codigo += " "+op+" ";
        if (der instanceof Temporal) 
        {
            codigo += ((Temporal)der).getNom();
        }else
        {
            codigo += der;
        }
        
        return codigo;
    }

    public String getContenido()
    {
        String codigo = "";
        if (izq instanceof Temporal) 
        {
            codigo += ((Temporal)izq).getNom()+" ";
        }else
        {
            codigo += izq+" ";
        }
        codigo += " "+op+" ";
        if (der instanceof Temporal) 
        {
            codigo += ((Temporal)der).getNom();
        }else
        {
            codigo += der;
        }
        return codigo;
    }
    
    @Override
    public String toString() 
    {
        return nom;
    }

    public Object getDer() 
    {
        return der;
    }

    public Object getIzq() {
        return izq;
    }

    public Object getOp() {
        return op;
    }

    public Temporal getTmp() {
        return tmp;
    }

    public void setDer(Object der) {
        this.der = der;
    }

    public void setIzq(Object izq) {
        this.izq = izq;
    }

    public void setOp(Object op) {
        this.op = op;
    }

    public void setTmp(Temporal tmp) {
        this.tmp = tmp;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
