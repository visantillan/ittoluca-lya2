
package Arbol;

public class Etiquetas 
{
    private String codigo;
    private String nombre = "";
    private String contenido;
    private int eVerdadera;
    private int eFalsa;

    public Etiquetas(String nombre) {
        this.nombre = nombre;
    }

    
    public Etiquetas(int eVerdadera, int eFalsa) {
        this.eVerdadera = eVerdadera;
        this.eFalsa = eFalsa;
    }
    
    public Etiquetas(String nombre, int eVerdadera, int eFalsa) {
        this.nombre = nombre;
        this.eVerdadera = eVerdadera;
        this.eFalsa = eFalsa;
    }

    public Etiquetas(int eVerdadera, int eFalsa, String contenido) {
        this.eVerdadera = eVerdadera;
        this.eFalsa = eFalsa;
        this.contenido = contenido;
    }
    
    
    
    public String desp()
    {
        return nombre+" "+eVerdadera+" "+eFalsa+" "+contenido;
    }
    

    
    
    /**
     * @return the eVerdadera
     */
    public int geteVerdadera() {
        return eVerdadera;
    }

    /**
     * @param eVerdadera the eVerdadera to set
     */
    public void seteVerdadera(int eVerdadera) {
        this.eVerdadera = eVerdadera;
    }

    /**
     * @return the eFalsa
     */
    public int geteFalsa() {
        return eFalsa;
    }

    /**
     * @param eFalsa the eFalsa to set
     */
    public void seteFalsa(int eFalsa) {
        this.eFalsa = eFalsa;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the contenido
     */
    public String getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
