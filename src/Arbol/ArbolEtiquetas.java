/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Arbol;
import Lexico.Lexema;
import Lexico.Lexico;
import java.util.List;

/**
 *
 * @author Geovanny
 */
public class ArbolEtiquetas 
{
    private Nodo raiz;

    public ArbolEtiquetas() {
    }

    private String codigoInter = "";
    String op ="";
    public void infijoRutinaLogica(Nodo raiz)
    {
        if (raiz != null) 
        {
            Etiquetas eti = (Etiquetas) raiz.getObj();
            infijoRutinaLogica(raiz.getIzq());
            if (!eti.getNombre().equals("OR") && !eti.getNombre().equals("AND")
                    && !eti.getContenido().contains("AND") && !eti.getContenido().contains("OR") ) 
            {
                List<Lexema> listaContenido = (List<Lexema>) new Lexico().separa(eti.getContenido());
                if (listaContenido.size()>3)
                {
                    if (codigoInter.trim().endsWith(":")) 
                    {
                        codigoInter += "\n";
                    }
                    List<Temporal> temporales = ArbolRutinas.generarTmp(listaContenido);
                    String condicion = temporales.get(temporales.size()-1).getContenido();
                    for (Temporal temporale : temporales) 
                    {
                        codigoInter += temporale.desp()+"\n";
                    }
                    codigoInter += "\nif ("+condicion+" )"+eti.getCodigo();
                }else
                {
                    codigoInter += "\nif ("+eti.getContenido()+" )"+eti.getCodigo();
                }
            }
            infijoRutinaLogica(raiz.getDer());
        }
    }
    
    public void intermedio(Nodo raiz)
    {
        if (raiz != null) 
        {
            Etiquetas eti = (Etiquetas) raiz.getObj();
            infijoRutinaLogica(raiz.getIzq());
            System.out.println(eti.desp());
            infijoRutinaLogica(raiz.getDer());
        }
    }
    /**
     * @return the raiz
     */
    public Nodo getRaiz() {
        return raiz;
    }

    /**
     * @param raiz the raiz to set
     */
    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }

    /**
     * @return the codigoInter
     */
    public String getCodigoInter() {
        return codigoInter;
    }

    /**
     * @param codigoInter the codigoInter to set
     */
    public void setCodigoInter(String codigoInter) {
        this.codigoInter = codigoInter;
    }
    
}
