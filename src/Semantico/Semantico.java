package Semantico;

import Lexico.ArchivoSecuencial;
import Lexico.Lexema;
import Lexico.Lexico;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;

/**
 *
 * @author Geovanny Elias Montiel Francisco
 */
public class Semantico
{
    private int indice = 0;
    private List<Propiedades> valor;
    private String tDato = Propiedades.SIN_TIPO_DATO;
    private String acceso = "";
    private List<Propiedades> parametros;
    
    private TablaSimbolos simbolos;

    /**
     * Constructor para instanciar el objeto de la clase TablaSimbolos
     */
    public Semantico() 
    {
        simbolos = new TablaSimbolos();
        valor = new ArrayList();
    }
    
    /**
     * Metodo para llenar una tabla con todos los elementos del codigo y sus correspondientes valores
     * (IMPORTANTE) 
     * 
     * @param codigo es el codigo fuente a analisar
     */
    @SuppressWarnings("UnusedAssignment")
    public void llenarTablaGeneral(String codigo)
    {
        Lexema lex[] = new Lexema[0];
        lex = new Lexico().separa(codigo).toArray(lex);
        
        for (indice = 0; indice < lex.length ; indice++) 
        {
            Lexema tmp = lex[indice];
            switch (tmp.getLexema()) 
            {
                case "CLASE":   //encontro palabla reservada clase
                    
                    tmp = lex[indice+1];    //avanzamos al lexema del nombre de la clase
                    acceso = Propiedades.GLOBAL;    //el acceso cambiara a global
                    
                    /*inserta el nombre de la clase en la tabla de simbolos*/
                    simbolos.inserta(tmp.getLexema(),tmp.getNoToken(),
                    Propiedades.CLASE, Propiedades.SIN_USO, Propiedades.SIN_VALOR, tmp.getLinea(),
                    tmp.getColumna(), parametros, Propiedades.GLOBAL, Propiedades.CLASE, Propiedades.SIN_METODO);
                    
                    indice = indice+2;  //avanzamos hasta despues del nombre de la clase
                    simbolos.setMetodoActual(Propiedades.SIN_METODO);   //el metodo actual sera "sin metodo"
                    tmp = lex[indice];  //avanzamos el lexema despues de la llave que habre en la clase "¿"
                    break;
                case "PRINCIPAL":
                    acceso = Propiedades.LOCAL; //el acceso sera local
                    simbolos.setMetodoActual("PRINCIPAL")   //el metodo actual sera PRINCIPAL
                    ;break;
                case "METODO":
                    
                    tmp = lex[indice+1];    //avanzamos hacia el nombre del metodo
                    acceso = Propiedades.LOCAL; //el acceso sera local
                    simbolos.setMetodoActual(tmp.getLexema());  //el metodo actual sera el que acabamos de encontrar
                    parametros = getParametros(lex, indice+3);  //optenemos los parametros del metodo
                    simbolos.getTablaParametros().addAll(parametros);

                    /*insertamos el nombre del metodo a la tabla de simbolos*/
                    simbolos.inserta(tmp.getLexema(),tmp.getNoToken(),
                        Propiedades.METODO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, tmp.getLinea(),
                        tmp.getColumna(), parametros, Propiedades.GLOBAL, Propiedades.METODO, simbolos.getMetodoActual());
                    
                    tmp = lex[indice];  //movemos el lexema hasta despues de los parametros del metodo
                    break;
            }
            //si encontro alguna sentencia que contiene valores
            if (tmp.getLexema().equals("SI") || tmp.getLexema().equals("MIENTRAS")
                    || tmp.getLexema().equals("IMPRIMIR") || tmp.getLexema().equals("LEER")) 
            {
                if (tmp.getLexema().equals("IMPRIMIR")) //si encontro la palabra reservada IMPRIMIR
                {
                    valor = getImprimir(lex, indice+1); //optenemos lo que contiene el metodo imprimir entre sus parentesis
                }else
                {
                    valor = getCondicion(lex, indice+1);  //optenemos todo lo que contiene dentro los parentesis de la sentencia
                }
                /*inserta en la tabla de simbolos la condicion de la sentencia que encontro*/
                simbolos.inserta(
                            Propiedades.SIN_LEXEMA,Propiedades.SIN_NO_TOKEN,
                            Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, valor, tmp.getLinea(),
                            tmp.getColumna(), new ArrayList(), Propiedades.LOCAL, Propiedades.CONDICION, simbolos.getMetodoActual());
                tmp = lex[indice];  //avanzamos hasta despues de donde haya terminado la
            }
            if (tmp.getNoToken()>=0 && tmp.getNoToken() <=4)    //si encontro un tipo de dato
            {
                tDato = tmp.getLexema();    //el tipo de dato cambiara al tipo de dato encontrado
            }
            if (tmp.getLexema().equals("LEER")) //si encontro la palabra reservada leer
            {
                //busca la variable que lee para cambiarle a que se uso
                simbolos.buscaGeneral(lex[indice+1].getLexema(), Propiedades.VARIABLE, simbolos.getMetodoActual());
                indice = indice+1;
                tmp = lex[indice];
            }
            else if (tmp.getNoToken() == 5)  //encontro una variable
            {   
                boolean esMetodo = false;
                if (lex[indice+1].getLexema().equals("(")) //si el siguente es un ( entonces es una llamada a un metodo
                {
//                    System.out.println("es metodo "+tmp.desp());
                    esMetodo = true;
                    //si la busqueda del metodo no es un error
                    if (!simbolos.buscaGeneral(tmp.getLexema(), Propiedades.METODO, tmp.getLexema()).equals("ERROR"))    
                    {
//                        System.out.println("si existe metodo");
                        List<Propiedades> lista1 = simbolos.getParametrosMetodo(tmp.getLexema());   //optenemos los paramtros del metodo que ya se habia creado
                        List<Propiedades> lista2 = getLlamadaMetodo(lex, indice+2); //optenemos los parametros del metodo que se esta llamando
                        
                        if (lista1.size() == lista2.size()) //si el numero de parametros es igual
                        {
                            if (comparaParametros(lista1, lista2)) //comparamos los tipos de dato de cada parametro
                            {
                                /*insertamos el metodo que fue llamado*/
                                simbolos.inserta(
                                tmp.getLexema(),tmp.getNoToken(),
                                Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, tmp.getLinea(),
                                tmp.getColumna(), lista2, Propiedades.LOCAL, Propiedades.METODO,Propiedades.SIN_METODO);
                                tmp = lex[indice];
                                continue;   //seguimos con la siguiente iteracion del for
                            }else
                            {
                                simbolos.getTablaErrores().add("Error, tipos de parametros invalidos en Linea:"+tmp.getLinea()+" Columna:"+tmp.getColumna());
                            }
                        }else
                        {
//                            System.out.println("no mismos cantidad de parametros");
                            simbolos.getTablaErrores().add("Error, numero de parametros invalido Linea:"+tmp.getLinea()+" Columna:"+tmp.getColumna());
                        }
                    }//fin llamada metodo
                    else
                    {
                        simbolos.getTablaErrores().add("Error, el metodo no existe Linea:"+tmp.getLinea()+" Columna:"+tmp.getColumna());
                        continue;   //que desde este punto corte el codigo y empieze en la siguiente iteracion del for
                    }
                }
                if (lex[indice+1].getLexema().equals("[")) 
                {   
                    System.out.println("hay corchetes");
                    System.out.println("******"+lex[indice+5].getLexema());
                    if (lex[indice+4].getLexema().equals("NUEVO")) 
                    {
                        String valorTamano = "";
                        valor.add(new Propiedades(tmp.getLexema(), tmp.getNoToken(), Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, tmp.getLinea(), tmp.getColumna(),new ArrayList(), Propiedades.LOCAL, Propiedades.VARIABLE, simbolos.getMetodoActual()));
                        if (lex[indice+8].getNoToken() == 5) 
                        {
                            
                            if (simbolos.buscaGeneral(lex[indice+8].getLexema(), Propiedades.VARIABLE, Propiedades.SIN_METODO).equals("ERROR")) 
                            {
                                simbolos.getTablaErrores().add("Error variable no declarada Linea "+lex[indice+7].getLinea());
                                indice = indice+7;
                                continue;
                            }else
                            {
                                valorTamano = patronToken(lex[indice+7].getNoToken(), lex[indice+7].getLexema(), Propiedades.VARIABLE, Propiedades.SIN_METODO);
                            }
                        }
                        else if (lex[indice+7].getNoToken() != 31) 
                        {
                            simbolos.getTablaErrores().add("Error tamaño del arreglo incorrecto Linea "+lex[indice+7].getLinea());
                            indice = indice+7;
                            continue;
                        }else
                        {
                            valorTamano = patronToken(lex[indice+7].getNoToken(), lex[indice+7].getLexema(), Propiedades.VARIABLE, Propiedades.SIN_METODO);
                        }
                                
                        valor.add(new Propiedades(lex[indice+3].getLexema(), indice, tDato, tDato, valor, indice, indice, parametros, acceso, codigo));
                        valor.add(new Propiedades("FALSO", 
                                lex[indice+5].getNoToken(),
                                lex[indice+5].getLexema(),
                                Propiedades.SIN_USO,
                                Propiedades.SIN_VALOR,
                                lex[indice+6].getLinea(),
                                lex[indice+6].getColumna(),
                                new ArrayList(),
                                Propiedades.LOCAL, 
                                Propiedades.VARIABLE, 
                                simbolos.getMetodoActual(),
                                new Integer(lex[indice+7].getLexema())));
                        System.out.println("]]]]]]]]]"+valor);
                    }else
                    {
                        valor = getValor(lex, indice+2);
                        indice = indice+2;
                    }
                    simbolos.inserta(
                            tmp.getLexema(),tmp.getNoToken(),
                            tDato, Propiedades.SIN_USO, valor, tmp.getLinea(),
                            tmp.getColumna(), new ArrayList(), acceso, Propiedades.VARIABLE, simbolos.getMetodoActual());
                    continue;
                }
                else if (lex[indice+1].getLexema().equals("="))  //si encontro un =
                {
                    valor = getValor(lex, indice);    //valor de la variable
                }else
                {
                    valor = Propiedades.SIN_VALOR;
                }
                /*insetamos la variable que acabamos de encontrar*/
                if (!esMetodo) 
                {
                    simbolos.inserta(
                            tmp.getLexema(),tmp.getNoToken(),
                            tDato, Propiedades.SIN_USO, valor, tmp.getLinea(),
                            tmp.getColumna(), new ArrayList(), acceso, Propiedades.VARIABLE, simbolos.getMetodoActual());
                }
                tmp = lex[indice];
            }
            if (tmp.getLexema().equals(";"))    //si encontro un punto y coma el tipo de dato regresara a vacio
            {
                tDato = Propiedades.SIN_TIPO_DATO;
            }
            tmp = lex[indice];  //avanzamos hasta donde se haya quedado el indice
        }//fin for
    }
    
    /**
     * Metodo que realiza el analisis Semantico de un codigo fuente
     * 
     * @param codigo es el codigo fuente que analisaremos
     * @return retornamos semanticamente esta bien o nop
     */
    public boolean analisaSemantico(String codigo)
    {
        llenarTablaGeneral(codigo); //llenamos la tabla general
        
        Evaluacion eva = new Evaluacion();  //objeto para utilizar su metodo de evalua
        Propiedades tmp;                    //variable de ayuda unicamente
        
        for (int i = 0; i < simbolos.getTablaGeneral().size(); i++) 
        {
            tmp = simbolos.getTablaGeneral().get(i);
            if (!tmp.getValor().isEmpty())  //si no esta vacio en su campo valor
            {
                if (checaValores(tmp.getValor()))   //si sus valores son correctos
                {
                    String valorAnterior = "";
                    List<Propiedades> lista = tmp.getValor();
                    for (Propiedades lista1 : lista) {
                        valorAnterior += lista1.getLexema()+" ";
                    }
                    if (eva.evaluacion(getFormato(tmp.getValor()))) //si la Evaluacion es correcta
                    {
                        tmp.getValor().clear();
                        tmp.getValor().add(new Propiedades(valorAnterior, indice, tDato, tDato, valor, indice, i, parametros, acceso, codigo));
                        
                        if (!tmp.getTipoSimbolo().equals(Propiedades.CONDICION)
                        && !tmp.getTipoDato().equals(Propiedades.SIN_TIPO_DATO)) {
                            simbolos.getTablaSimbolos().add(tmp);
                        }
                        if (tmp.getTipoDato().equals(Propiedades.SIN_TIPO_DATO)) 
                        {
                            simbolos.cambiaValor(tmp.getLexema(),tmp.getAcceso(), tmp.getTipoSimbolo(),tmp.getValor());
                        }
                    }
                    else
                    {
//                        System.out.println(eva.evaluacion(getFormato(tmp.getValor())));
//                        System.out.println("* "+tmp.getTipoSimbolo());
                        simbolos.getTablaErrores().add("Error tipos no compatibles Linea:"+tmp.getNoLinea()+" Columna:"+tmp.getColumna());
                    }
                }
            }
            else if(tmp.getTipoSimbolo().equals(Propiedades.VARIABLE) 
                    || tmp.getTipoSimbolo().equals(Propiedades.METODO)
                    || tmp.getTipoSimbolo().equals(Propiedades.CLASE))  //si no es una condicion
            {
                if (!simbolos.busca(tmp.getLexema(), tmp.getAcceso()).equals("ERROR"))  //si la busqueda no produjo un error
                {
                    simbolos.getTablaSimbolos().add(tmp);
                }else
                {
                    if (!tmp.getTipoSimbolo().equals(Propiedades.METODO)) {
                        simbolos.getTablaErrores().add("Error, variable "+tmp.getLexema()+" no declarada Linea:"+tmp.getNoLinea()+" Columna:"+tmp.getColumna());
                    }
                }
            }
        }
        varNoUsadas();  //llamamos al metodo que detemina si no fueron usadas
        
        /*ordenamos la tabla de simbolos*/
        List<Propiedades> tablaSimbolosOrdenada = ordenaTabla(simbolos.getTablaSimbolos());
        simbolos.setTablaSimbolos(tablaSimbolosOrdenada);   //le asignamos la tabla ya ordenada
        
        if (simbolos.getTablaErrores().isEmpty()) 
        {
            return true;
        }
        return false;
    }
    
    /**
     * Metodo para determinar que variables no fueron usadas en el codigo
     */
    private void varNoUsadas()
    {
        Propiedades tmp;
        for (int i = 0; i < simbolos.getTablaSimbolos().size(); i++) 
        {
            tmp = simbolos.getTablaSimbolos().get(i);
            if (tmp.getUso().equals(Propiedades.SIN_USO) 
                    && tmp.getTipoSimbolo().equals(Propiedades.VARIABLE)
                    && !tmp.getTipoDato().equals(Propiedades.SIN_TIPO_DATO))   //si no se uso
            {
                simbolos.getTablaErrores().add("Error, variable no usada Linea: "+tmp.getNoLinea()+" Columna:"+tmp.getColumna());
            }
        }
    }
    
    /**
     * Metodo para dar formato a los valores 
     * 
     * @param valores es el valor que vamos a dar formato
     * @return retorna un arreglo con el formato que deseamos
     */
    private Propiedades[] getFormato(List<Propiedades> valores)
    {
        Posfijo pos = new Posfijo();                                        //objeto para realizar el postfijo
        Object arreglo [] =  pos.posfijo(valores.toArray());                //arreglo ya ordenado en postfijo
        Propiedades arregloFormato [] = new Propiedades[arreglo.length];    //arreglo que tendra el formato adeacuado para Evaluacion
        Propiedades tmp;
        for (int i = 0; i < arreglo.length; i++) 
        {
            tmp = (Propiedades) arreglo[i];
            if (tmp.getNoToken() == 5)  //si es una variable
            {
//                System.out.println(tmp.desp());
                tmp.setTipoDato(patronToken(tmp.getNoToken(), tmp.getLexema(), Propiedades.VARIABLE, tmp.getMetodo()));
            }else
            {
                tmp.setTipoDato(patronToken(tmp.getNoToken(), tmp.getLexema(),tmp.getTipoSimbolo(), tmp.getMetodo()));
            }
            arregloFormato[i]= tmp;
        }
        return arregloFormato;
    }
    
    /**
     * Metodo para checar los valores de la tabla de simbolos
     * 
     * @param valores es una lista con todos los elementos de de una expresion
     * @return retorna falso cuando no encontro una varible
     */
    private boolean checaValores(List<Propiedades> valores)
    {
        Propiedades tmp;    //variable de ayuda para cada elemento de la lista
        boolean declarada = true;
        for (int i = 0; i < valores.size(); i++) 
        {
            tmp = valores.get(i);
            if (tmp.getNoToken()==5) //si el valor es una variable
            {
                if (simbolos.buscaGeneral(tmp.getLexema(), Propiedades.VARIABLE, tmp.getMetodo()).equals("ERROR")) //si el resultado de la busqueda es un error
                {
                    if(simbolos.buscaGeneral(tmp.getLexema(), Propiedades.VARIABLE, Propiedades.SIN_METODO).equals("ERROR")) //si el resultado de la busqueda es un error)
                    {
                        simbolos.getTablaErrores().add("Error,variable "+tmp.getLexema()+" no declarada en Linea:"+tmp.getNoLinea()+" Columna:"+tmp.getColumna());
                        declarada = false;
                    }
                }
            }
        }
        return declarada;     
    }
    
    /**
     * Metodo para ordenar los errores en una secuencia
     * @param tabla es la lista que vamos a ordenar
     * @return retornamos la lista ya ordenada
     */
    private List<Propiedades> ordenaTabla(List<Propiedades> tabla)
    {
        List<Propiedades> tablaOrdenada = tabla;
        
        Propiedades tmp;    //variable para guardar la informacion de un dato antes de ordenarlo con otro
        for (int i = 0; i < (tablaOrdenada.size()-1); i++) 
        {
            for (int j = i; j < (tablaOrdenada.size()-1); j++) 
            {
                if (tablaOrdenada.get(j).getNoLinea()>tablaOrdenada.get(j+1).getNoLinea())  //si el primero es mayor que el segundo
                {
                    tmp = tablaOrdenada.get(j); //guardamos la informacion del primer elemento
                    tablaOrdenada.set(j, tablaOrdenada.get(j+1));   //cambiamos el valor menor por el menor
                    tablaOrdenada.set(j+1, tmp);    //cambiamos el valor mayor por el menos
                }
            }
        }
        return tablaOrdenada;
    }
    
    /**
     * Metodo para comparar los parametros de dos metodos
     * 
     * @param parametro1 y parametro2 son para comparar sus tipos de datos
     * @return retorn si los parametros son iguales o nop
     */
    private boolean comparaParametros(List<Propiedades> parametros1, List<Propiedades> parametros2)
    {
        if (parametros1 != null && parametros2 != null) 
        {
            for (int i = 0; i < parametros1.size(); i++) 
            {
                
                if (!parametros1.get(i).getTipoDato().equals(parametros2.get(i).getTipoDato())) //si los parametros no son validos
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Metodo que optiene el valor de una declaracion
     * 
     * @param arreglo es un arreglo de lexemas para llenar el valor
     * @return retorna el valor como una sola cadena
     */
    private List<Propiedades> getValor(Lexema arreglo [], int pos)
    {
        List<Propiedades> valorTmp = new ArrayList();
        for (int i = pos; i < arreglo.length; i++) 
        {
            if (!arreglo[i].getLexema().equals(",") && !arreglo[i].getLexema().equals(";")) 
            {
                valorTmp.add(new Propiedades(arreglo[i].getLexema(), arreglo[i].getNoToken(), Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, arreglo[i].getLinea(), arreglo[i].getColumna(),new ArrayList(), Propiedades.LOCAL, Propiedades.VARIABLE, simbolos.getMetodoActual()));
            }
            else
            {
                indice = i;
                break;
            }
        }
//        System.out.println("");
        return valorTmp;
    }
    
    /**
     * Metodo para optener el valor de una impresion
     * 
     * @param arreglo es el arreglo de lexemas, pos es la posicion donde empezara a buscar
     * @return retorna una lista de con los valores de una impresion
     */
    private List<Propiedades> getImprimir(Lexema arreglo [], int pos)
    {
        List<Propiedades> imprime = new ArrayList();
        
        for (int i = pos; i < arreglo.length; i++) 
        {
            if (!arreglo[i].getLexema().equals(";"))    //si no encontro parantesis
            {
                imprime.add(new Propiedades(arreglo[i].getLexema(), arreglo[i].getNoToken(), Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, arreglo[i].getLinea(), arreglo[i].getColumna(),new ArrayList(), Propiedades.LOCAL, Propiedades.CONDICION, simbolos.getMetodoActual()));
            }else
            {
                indice = i;
                break;
            }
        }
        return imprime;
    }
    
    /**
     * Metodo para obtener la condicion de un determinado sentencia
     * 
     * @param arreglo es un arreglo de lexemas para llenar la condicion
     * @return retorn la condicion como una sola cadena
     */
    private List<Propiedades> getCondicion(Lexema arreglo[], int pos)
    {
        List<Propiedades> condicion = new ArrayList();
        for (int i = pos; i < arreglo.length; i++) 
        {
            if (!arreglo[i].getLexema().equals("¿")) //si no encuentra ) que siga agregando
            {
                condicion.add(new Propiedades(arreglo[i].getLexema(), arreglo[i].getNoToken(), Propiedades.SIN_TIPO_DATO, Propiedades.SIN_USO, Propiedades.SIN_VALOR, arreglo[i].getLinea(), arreglo[i].getColumna(),new ArrayList(), Propiedades.LOCAL, Propiedades.CONDICION, simbolos.getMetodoActual()));
            }else
            {
                this.indice = i;
                break;
            }
        }
        return condicion;
    }
    
    /**
     * Metodo que optiene los parametros de un metodo que es llamado
     * 
     * @param arreglo es el arreglo de lexemas, pos es la posicion donde empezara a buscar
     * @return retorna una lista con objetos de la clase Propiedades (una lista de parametros) de una llamada a un metodo
     */
    private List<Propiedades> getLlamadaMetodo(Lexema arreglo[], int pos)
    {
        String nomMetodo = arreglo[pos].getLexema();
        List<Propiedades> param = new ArrayList();
        for (int i = pos; i < arreglo.length; i++) 
        {
            if (!arreglo[i].getLexema().equals(")")) 
            {
                if (!arreglo[i].getLexema().equals(",")) 
                {
                    String tipoDato = patronToken(arreglo[i].getNoToken(),arreglo[i].getLexema(), Propiedades.VARIABLE, simbolos.getMetodoActual());
                    param.add(new Propiedades(arreglo[i].getLexema(), arreglo[i].getNoToken(), tipoDato, Propiedades.SIN_USO, Propiedades.SIN_VALOR, arreglo[i].getLinea(), arreglo[i].getColumna(), Propiedades.SIN_VALOR, Propiedades.LOCAL, Propiedades.VARIABLE));
                }
            }else
            {
                indice = pos;
                break;
            }
        }
        return param;
    }
    
    /**
     * Metodo para optener los parametros de un metodo
     * 
     * @param arreglo es el arreglo de lexemas, pos es la posicion donde empezara a buscar
     * @return retorn una lista con objetos de la clase Propiedades (una lista de parametros)
    */
    private List<Propiedades> getParametros(Lexema[] arreglo,int pos)
    {
        String tipoDato = "";
        boolean duplicadas = false; 
        List<Propiedades> param = new ArrayList();
        for (int i = pos; i < arreglo.length; i++) 
        {
            if (!arreglo[i].getLexema().equals(")")) //si no encuentra ) que siga agregando
            {
                if (arreglo[i].getNoToken() >= 1 && arreglo[i].getNoToken() <= 4) // si es un tipo de dato
                {
                    tipoDato = arreglo[i].getLexema();
                }
                else if (arreglo[i].getNoToken() == 5) //si es una variable
                {
                    for (int j = 0; j < param.size(); j++)
                    {
                        if (arreglo[i].getLexema().equals(param.get(j).getLexema())) //si dos parametros tienen el mismo nombre
                        {
                            duplicadas = true;
                            break;
                        }
                    }
                    if (!duplicadas) 
                    {
                        param.add(new Propiedades(arreglo[i].getLexema(), arreglo[i].getNoToken(), tipoDato, Propiedades.SIN_USO, Propiedades.SIN_VALOR, arreglo[i].getLinea(), arreglo[i].getColumna(),new ArrayList(), Propiedades.LOCAL, Propiedades.PARAMETRO,simbolos.getMetodoActual()));
                    }else
                    {
                        simbolos.getTablaErrores().add("Error, variable "+arreglo[i].getLexema()+" duplicada Linea:"+arreglo[i].getLinea()+" Columna:"+arreglo[i].getColumna());
                    }
                }
            }else
            {
                this.indice = i + 1;
                break;
            }
        }
        return param;
    }
    
    /**
     * Metodo para saber el patron del token
     * @param noToken numero de token del lexema
     * @return retorn el patron del respectivo token
     */
    public String patronToken(int noToken, String lexema, String tipoSimbolo,String metodo)
    {
        String tokenPalabra;
        switch (noToken) 
        {
            case 5: tokenPalabra = simbolos.buscaGeneral(lexema, tipoSimbolo, metodo);break;
            case 31:tokenPalabra = "ENTERO";break;
            case 32:tokenPalabra = "DECIMAL";break;
            case 35:tokenPalabra = "CADENA";break;
            case 38: tokenPalabra = "BOOLEANO";break; 
            case 37: tokenPalabra = "BOOLEANO";break; 
                default: tokenPalabra = Propiedades.SIN_TIPO_DATO;
                break;
        }
//        System.out.println("Resultado patronToken "+tokenPalabra);
        return tokenPalabra;
    }
    
    public static void main(String[] args) 
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) 
        {
            ArchivoSecuencial arch = new ArchivoSecuencial(chooser.getSelectedFile().toString());
            Semantico sem = new Semantico();
            sem.analisaSemantico(arch.leer());
            System.out.println("TABLA GENERAL");
            List<Propiedades> tablaGeneral = sem.simbolos.getTablaGeneral();
            for (int i = 0; i < tablaGeneral.size(); i++) 
            {
                System.out.println(tablaGeneral.get(i).desp());
            }
            System.out.println("TABLA SIMBOLOS");
            List<Propiedades> tablaSimbolos = sem.simbolos.getTablaSimbolos();
            for (int i = 0; i < tablaSimbolos.size(); i++) 
            {
                System.out.println(tablaSimbolos.get(i).desp());
            }
            
            System.out.println("\nTABLA ERRORES");
            List<String> tablaErrores = sem.simbolos.getTablaErrores();
            for (int i = 0; i < tablaErrores.size(); i++) 
            {
                System.out.println(tablaErrores.get(i));
            }
        }
    }
}
