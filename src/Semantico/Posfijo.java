package Semantico;

import Arbol.ArbolRutinas;
import Arbol.Temporal;
import Lexico.Lexema;
import Lexico.Lexico;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * Clase para realizar el metodo de Posfijo de una experesion
 */
public class Posfijo 
{

    /**
     * Metodo que realiza el Posfijo de una expresion
     * 
     * @param arreglo es un arreglo con todos los elementos de una expresion
     * @return retorna un arreglo de la expresion ordenada en Posfijo
     */
    public Object[] posfijo(Object arreglo[]) 
    {
        boolean bandera;
        Stack<Object> pila = new Stack<>();
        Queue<Object> cola = new LinkedList();
        
        //EVALUA LOS ELEMENTOS DEL ARREGLO
        for (int i = 0; i < arreglo.length; i++) 
        {
            Object elemento = arreglo[i];
            
            if (elemento.toString().equals("(")) 
            {
                pila.push(elemento);
            } 
            else 
            {
                if (elemento.toString().equals("+") | elemento.toString().equals("-") | elemento.toString().equals("*") 
                    | elemento.toString().equals("/") | elemento.toString().equals("<") | elemento.toString().equals(">")
                    | elemento.toString().equals("<=") | elemento.toString().equals(">=") | elemento.toString().equals("!=")
                    | elemento.toString().equals("AND") | elemento.toString().equals("OR") | elemento.toString().equals("==")
                    | elemento.toString().equals("=")) 
                {
                    if (!pila.empty()) 
                    {
                        if (pila.peek().toString().equals("(") || pila.peek().toString().equals("AND") || pila.peek().toString().equals("OR")) 
                        {
                            pila.push(elemento);
                        } 
                        else 
                        {
                            if (prioridad(pila.peek().toString()) >= prioridad(elemento.toString())) 
                            {
                                cola.add(pila.pop());
                                pila.push(elemento);
                            } 
                            else 
                            {
                                if (pila.peek().toString().equals("=")) 
                                {
                                    pila.add(elemento);
                                }
                                else 
                                {
                                    pila.push(elemento);
                                }

                            }
                        }
                    }
                    else 
                    {
                        pila.push(elemento);
                    }
                }// termina condicion de operador
                else 
                {
                    if (elemento.toString().equals(")")) 
                    {
                        bandera = false;
                        while (bandera == false & !pila.empty()) 
                        {
                            if (!pila.peek().toString().equals("("))
                            {
                                cola.add(pila.pop());
                            } 
                            else 
                            {
                                bandera = true;
                                pila.pop();
                            }
                        }
                    } 
                    else 
                    {
                        cola.add(elemento);
                    }
                }
            }
        }
        while (!pila.empty()) 
        {
            cola.add(pila.pop());
        }
        Object vector[] = cola.toArray(new Object[0]);
        return vector;
    }

    /**
     * Metodo para saber la prioridad de un operador 
     * 
     * @param operador es para poder darle una prioridad de acuerdo al operador
     * @return retorna en un entero la prioridad
     */
    public int prioridad(String operador) 
    {
        int prioridad = -1; 
        
        if (operador.equals("*") | operador.equals("/")) 
            prioridad = 4;
        
        if (operador.equals("+") | operador.equals("-")) 
            prioridad = 3;
        
        if (operador.equals("<") | operador.equals(">") | operador.equals("<=") | operador.equals(">=") | operador.equals("==")) 
            prioridad = 2;
        
        if (operador.equals("AND") | operador.equals("OR") | operador.equals("NOT")  | operador.equals("!=")) 
            prioridad = 1;
        
        if (operador.equals("=")) 
            prioridad = 0;
        
        return prioridad;
    }

    public static void main(String[] args) {
//        int a = 0;
//        float b = 0;
//        propiedades obj1 = new propiedades("(", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj2 = new propiedades("(", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj3 = new propiedades("a", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj4 = new propiedades("SUMA", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj5 = new propiedades("b", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj6 = new propiedades(")", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj7 = new propiedades("DIV", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj8 = new propiedades("c", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj9 = new propiedades(")", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj10 = new propiedades("MUL", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj11 = new propiedades("d", 1, "", "","", 1, 1, null, "", "");
//        propiedades obj12 = new propiedades(")", 1, "", "","", 1, 1, null, "", "");
//        Object arreglo[] = new propiedades[]{obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8,obj9,obj10,obj11,obj12};
//        
//        Posfijo pos = new Posfijo();
//        Object Posfijo[] = pos.Posfijo(arreglo);
//        
//        propiedades temp;
//        for (int i = 0; i < Posfijo.length; i++) 
//        {
//            temp = (propiedades) Posfijo[i];
//            System.out.println(temp.desp());
//        }
        
//        JFileChooser chooser = new JFileChooser();
//        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
//        
//        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) 
//        {
//            ArchivoSecuencial arch = new ArchivoSecuencial(chooser.getSelectedFile().toString());
            Posfijo pos = new Posfijo();
            Lexico lex = new Lexico();
            String cadena = "(a*10-2/2*(0-2)<a AND c>2)";
            Object arreglo [] = pos.posfijo(lex.separa(cadena).toArray());
            
            List<Lexema> lista = new ArrayList();
            for (int i = 0; i < arreglo.length; i++) 
            {
                System.out.print(arreglo[i]+" ");
                lista.add((Lexema) arreglo[i]);
            }
            System.out.println("");
            List<Temporal> tmp = ArbolRutinas.generarTmp(lista);
            for (Temporal tmp1 : tmp) 
            {
                System.out.println(tmp1.desp());
            }
//        }
    }
}
