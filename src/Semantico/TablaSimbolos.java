/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Semantico;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Geovanny Elias Montiel Francisco,
 *         Giovanny Castillo Estrada
 */
public class TablaSimbolos 
{
    private String metodoActual;
    private List<String> tablaErrores;
    private List<Propiedades> tablaGeneral;
    private List<Propiedades> tablaSimbolos;
    private List<Propiedades> tablaParametros;

    /**
     * Constructor para instaciar los objetos de las listas
     */
    public TablaSimbolos()
    {
        metodoActual = "";
        tablaErrores = new ArrayList();
        tablaGeneral = new ArrayList();
        tablaSimbolos = new ArrayList();
        tablaParametros = new ArrayList();
    }

    /**
     * Metodo para insertar en la tabla general
     * 
     */
    public void inserta(String lexema, int noToken, String tipoDato, String uso, List<Propiedades> valor, int noLinea, int columna, List<Propiedades> parametros,String acceso, String tipoSimbolo, String metodo)
    {
        System.out.println("metodo insertara "+lexema+" "+tipoDato);
        boolean banderaFor = true;
        boolean varDuplicada = false;
        for (int i = 0; i < tablaGeneral.size() && banderaFor ; i++) 
        {
            for (int j = 0; j < tablaParametros.size(); j++)    //for para la tabla de parametros
            {
                if (metodoActual.equals(tablaParametros.get(j).getMetodo()))//comprobamos el metodo actual
                {
                    if (lexema.equals(tablaParametros.get(j).getLexema())) //comprobamos si la variable local es igual q unos de los parametros
                    {
                        System.out.println("var duplicada");
                        varDuplicada = true;
                        banderaFor = false;
                    }
                }
            }
//            si la que se va a insertar no tiene el mismo nombre y que tenga un tipo de dato y el acceso sea el mismo
            if (!tipoDato.equals(Propiedades.SIN_TIPO_DATO)
                    && valor.isEmpty()) //si su tipo de dato no esta vacio
            {
                System.out.println("es una declaracion");
//                System.out.print(lexema+" "+noToken+" "+tipoDato+" "+uso);
//                System.out.println(valor+" "+acceso+" "+tipoDato+" "+metodo);
                if (lexema.equals(tablaGeneral.get(i).getLexema())  //si tienen el mismo nombre
                        && tipoSimbolo.equals(tablaGeneral.get(i).getTipoSimbolo()) // si son del mismo tipo
                        && metodo.equals(tablaGeneral.get(i).getMetodo()))  // si esta en el mismo metodo
                {
                    
                    System.out.println("var duplicada por mismo nombre");
                    varDuplicada = true;
                    banderaFor = false;
                }
            }
        }
        if (!varDuplicada)
        {
            System.out.println("var insertada correctamente");
            Propiedades obj = new Propiedades(lexema, noToken, tipoDato, uso, valor, noLinea, columna, parametros, acceso, tipoSimbolo, metodo);
            tablaGeneral.add(obj);
        } else
        {
            getTablaErrores().add("Error, variable " + lexema + " duplicada Linea:" + noLinea + " Columna:" + columna);
        }
    }

    /**
     * Metodo para buscar en la tabla de simbolos
     *
     * @param lexema es el nombre de la variable que va a buscar
     * @return retorna el tipo de dato de la variable buscada
     */
    public String busca(String lexema, String acceso) 
    {
        for (int i = 0; i < tablaGeneral.size(); i++) 
        {
            if (tablaGeneral.get(i).getLexema().equals(lexema) 
                    && !tablaGeneral.get(i).getTipoDato().equals(Propiedades.SIN_TIPO_DATO) 
                    && tablaGeneral.get(i).getAcceso().equals(acceso)
                    
                    ) 
            {
//                tablaGeneral.get(i).setUso("SI");
                return tablaGeneral.get(i).getTipoDato();
            }
        }
        return "ERROR";
    }
    /**
     * Metodo para buscar en la tabla de simbolos
     *
     * @param lexema es el nombre de la variable que va a buscar
     * @return retorna el tipo de dato de la variable buscada
     */
    
    public void cambiaValor(String lexema, String acceso, String tipoSimbolo, List<Propiedades> valor) 
    {
        for (int i = 0; i < tablaSimbolos.size(); i++) 
        {
            if (tablaSimbolos.get(i).getLexema().equals(lexema) 
                    && !tablaSimbolos.get(i).getTipoDato().equals(Propiedades.SIN_TIPO_DATO) 
                    && tablaSimbolos.get(i).getTipoSimbolo().equals(Propiedades.VARIABLE)
//                    && tablaSimbolos.get(i).getAcceso().equals(acceso)
                    
                    ) 
            {
                tablaSimbolos.get(i).setValor(valor);
            }
        }
    }
    
    /**
     * Metodo para buscar en la tabla de simbolos
     * elementos diferentes de las variables
     *
     * @param lexema es el nombre de la variable que va a buscar
     * @return retorna el tipo de dato de la variable buscada
     */
    public String buscaGeneral(String lexema, String tipoSimbolo, String metodo) 
    {
//        System.out.println("buscando "+lexema+" que es un "+tipoSimbolo+" y pertenece al metodo "+metodo);
//        System.out.println("");
        for (int i = 0; i < tablaGeneral.size(); i++) 
        {
//            System.out.println("analisando "+tablaGeneral.get(i).getLexema()+
//                               " con tipoSimbolo "+tablaGeneral.get(i).getTipoSimbolo()+
//                               " perteneciente al metodo "+tablaGeneral.get(i).getMetodo()+
//                               " con tipo dato "+tablaGeneral.get(i).getTipoDato());
//            System.out.print("nombres iguales ?"+tablaGeneral.get(i).getLexema().equals(lexema)+";");
//            System.out.print(" simbolosiguales ?"+tablaGeneral.get(i).getTipoSimbolo().equals(tipoSimbolo)+";");
//            System.out.print(" metodosiguales ?"+tablaGeneral.get(i).getMetodo().equals(metodo)+";");
//            System.out.println(" con tipo dato ?"+tablaGeneral.get(i).getLexema().equals(lexema));
//            System.out.println("");
            if (tablaGeneral.get(i).getLexema().equals(lexema) //si tienen el mismo nombre
                    && tablaGeneral.get(i).getTipoSimbolo().equals(tipoSimbolo) //si son del mismo tipo de simbolos
                    && (tablaGeneral.get(i).getMetodo().equals(metodo) 
                    || tablaGeneral.get(i).getAcceso().equals(Propiedades.GLOBAL))   //si pertenecen al mismo metodo
                    && !tablaGeneral.get(i).getTipoDato().equals(Propiedades.SIN_TIPO_DATO)) //si es una variable declarada
            {
//                System.out.println("encontradamos "+lexema);
                tablaGeneral.get(i).setUso("SI");
                return tablaGeneral.get(i).getTipoDato();
            }
        }
        return "ERROR";
    }
    
    /**
     * Metodo para buscar un metodo dentro de la tabla
     * 
     * @param lexema es el nombre del metodo que va a buscar
     * @return retorna los parametros del metodo
     */
    public List<Propiedades> getParametrosMetodo(String lexema) 
    {
        List<Propiedades> parametros = new ArrayList();
        for (int i = 0; i < tablaGeneral.size(); i++) 
        {
            if (tablaGeneral.get(i).getLexema().equals(lexema)) 
            {
                parametros = tablaGeneral.get(i).getParametros();
                return parametros;
            }
        }
        return parametros;
    }
    
    /**
     * Metodo para determinar que variables fueron usadas
     * 
     */
    public void noUsadas()
    {
        for (int i = 0; i < tablaGeneral.size() ; i++) 
        {
            if (!tablaGeneral.get(i).getUso().startsWith("SI") && tablaGeneral.get(i).getTipoDato()!="" && tablaGeneral.get(i).getTipoSimbolo() != "METODO") 
            {
                getTablaErrores().add("Error, variable "+tablaGeneral.get(i).getLexema()+"no usadada Linea:"+tablaGeneral.get(i).getNoLinea()+" Columna:"+tablaGeneral.get(i).getColumna());
            }
        }
    }
    
    /**
     * GETERS Y SETERS
     */
    
    public String getMetodoActual() {
        return metodoActual;
    }

    public List<Propiedades> getTablaGeneral() {
        return tablaGeneral;
    }

    public List<Propiedades> getTablaSimbolos() {
        return tablaSimbolos;
    }

    public void setMetodoActual(String metodoActual) {
        this.metodoActual = metodoActual;
    }

    public void setTabla(List<Propiedades> tabla) {
        this.tablaGeneral = tabla;
    }

    public void setTablaSimbolos(List<Propiedades> tablaSimbolos) {
        this.tablaSimbolos = tablaSimbolos;
    }

    public List<Propiedades> getTablaParametros() {
        return tablaParametros;
    }

    public void setTablaParametros(List<Propiedades> tablaParametros) {
        this.tablaParametros = tablaParametros;
    }

    public List<String> getTablaErrores() {
        return tablaErrores;
    }

    public void setTablaErrores(List<String> tablaErrores) {
        this.tablaErrores = tablaErrores;
    }
}
