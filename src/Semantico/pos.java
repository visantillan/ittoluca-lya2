package Semantico;

import Arbol.ArbolRutinas;
import Arbol.Temporal;
import Lexico.Lexema;
import Lexico.Lexico;
import Rutinas.Rutina;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class pos 
{
    
    public List<Lexema> toPostfijo(List<Lexema> listaInfija)
    {
        List<Lexema> nuevaLista = new ArrayList();
        if (listaInfija != null) 
        {
            if (listaInfija.size()>0) 
            {
                Stack<Lexema> pila = new Stack();
                Queue<Lexema> cola = new LinkedList();
                
                for (Lexema l : listaInfija) 
                {
                    System.out.println("elemento a evaluar "+l);
                    if (l.getLexema().equals("(")) 
                    {
                        pila.add(l);
                        System.out.println("se agrega a la pila");
                    }
                    else if (l.getLexema().equals(")")) 
                    {
                        System.out.println("vacia la pila porque encontro )");
                        while(!pila.isEmpty())
                        {
                            if (pila.peek().getLexema().equals("(")) 
                            {
                                pila.pop();
                                break;
                            }
                            cola.add(pila.pop());
                        }
                    }
                    else if (prioridad(l.getLexema())>0) 
                    {
                        if (!pila.isEmpty())
                        {
                            if (pila.peek().getLexema().equals("(")) 
                            {
                                System.out.println("se agrega ya que no hay operadores");
                                pila.add(l);
                            }
                            else if (prioridad(l.getLexema()) == prioridad(pila.peek().getLexema())) 
                            {
                                System.out.println(l+" igual prio "+pila.peek());
                                cola.add(pila.pop());
                                pila.add(l);
                            }
                            else if (prioridad(l.getLexema()) > prioridad(pila.peek().getLexema())) 
                            {
                                System.out.println(l+" mayor prio "+pila.peek());
                                pila.add(l);
                            }
                            else if (prioridad(l.getLexema()) < prioridad(pila.peek().getLexema())) 
                            {
                                System.out.println(l+" menor prio "+pila.peek());
                                while(!pila.isEmpty())
                                {
                                    if (pila.peek().getLexema().equals("(")) 
                                    {
                                        pila.pop();
                                        break;
                                        
                                    }
                                    cola.add(pila.pop());
                                }
                                pila.add(l);
                            }
                        }else
                        {
                            System.out.println("pila vacia, se agregara "+l);
                            pila.add(l);
                        }
                    }else
                    {
                        System.out.println("no es operdor, se agrega a la cola "+l);
                        cola.add(l);
                    }
                    System.out.println("PILA "+pila);
                    System.out.println("COLA "+cola);
                }
                while (!pila.isEmpty()) 
                {                    
                    cola.add(pila.pop());
                }
                nuevaLista = (List<Lexema>) cola;
            }
        }
        return nuevaLista;
    }
    public int prioridad(String operador)
    {
        int prioridad = -1;
        if (operador.equals("<") || operador.equals(">") 
                || operador.equals(">=") || operador.equals("<=")
                || operador.equals("==") || operador.equals("=")) 
        {
            prioridad = 4;
        }
        else if(operador.equals("AND") || operador.equals("OR"))
        {
            prioridad = 3;
        }
        else if(operador.equals("*") || operador.equals("/"))
        {
            prioridad = 2;
        }else if(operador.equals("+") || operador.equals("-"))
        {
            prioridad = 1;
        }
        return prioridad;
    }
    
    public static void main(String[] args) 
    {
        /*b 0 1 * +*/
        /*[2 1 * 2 /] [2, 1, *, 2, / ]*/
        /*2 1 < C 1 < AND*/
        /*
        a 10 * 2 2 / 0 2 - a < * - c 2 > AND 
        T1 = a  * 10
        T2 = 2  / 2
        T3 = 0  - 2
        T4 = T2  * T3
        T5 = T4  < a
        T6 = c  > 2
        T7 = T5  AND T6
        T8 = T1  - T7
        */
        /*
        a 10 * 2 2 / 0 2 - * a < c 2 > AND - ( 
        T1 = 2  * 2
        T2 = 0  / 2
        T3 = T2  * a
        T4 = c  < 2
        T5 = T3  > T4
        T6 = T5  - (
        T7 = T1  AND T6
        T8 = 10  - T7
        */
        String cadena = "(a*10-2/2*(0-2)<a AND c>2)";
        List<Lexema> lista = (List<Lexema>) new Lexico().separa(cadena);
        System.out.println(Rutina.listaToString(new pos().toPostfijo(lista)));
        List<Temporal> tmp = ArbolRutinas.generarTmp(lista);
        for (Temporal tmp1 : tmp) 
        {
            System.out.println(tmp1.desp());
        }
        
    }
}
