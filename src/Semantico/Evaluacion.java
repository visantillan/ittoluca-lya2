package Semantico;

import Lexico.ArchivoSecuencial;
import Lexico.Lexico;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;

/**
 * @author Geovanny Elias Montiel Francisco
 *         Giovany Castillo Estrada
 * 
 * Clase para realizar la Evaluacion de un expresion
 */
public class Evaluacion extends Posfijo
{
    int prioridad;
    Propiedades op1,op2;
    List<Object> lista;
   
    
    /**
     * Constructor por defecto de la clase
     */
    public Evaluacion() 
    {
        lista = new ArrayList();
    }
    
    /**
     * Metodo que realiza la Evaluacion de una expresion
     * @param arreglo es un arreglo ya ordenado en Posfijo para que el metodo lo evalue
     * @return retorna si la expresion es correpta o nop
     */
    public boolean evaluacion(Object arreglo[])
    {
        String tipoDato;
        lista = getLista(arreglo);
        Propiedades tmp;
        
        System.out.println("Datos a evaluar");
        for (int i = 0; i < lista.size(); i++) 
        {
            System.out.println(((Propiedades)lista.get(i)).desp()+" ");
        }
        System.out.println("--------------------");
        
        for (int i = 0; i < lista.size(); i++) 
        {
            System.out.println("FOR INTERNO EVA");
            tmp = (Propiedades) lista.get(i);
            prioridad = prioridad(tmp.getLexema());

            if (prioridad>=0 && prioridad <=4) 
            {
                op1 = (Propiedades) lista.get(i-2);
                op2 = (Propiedades) lista.get(i-1);
                
                System.out.println("evaluando expresion");
                
                tipoDato = tipo(op1.getTipoDato(), op2.getTipoDato(), tmp.getLexema()).toString();
                
                System.out.println("("+op1.getTipoDato()+" "+tmp.getLexema()+" "+op2.getTipoDato()+") = "+tipoDato);
                
                if (!tipoDato.equals("ERROR")) 
                {
//                    lista.remove(i-2);
//                    lista.remove(i-2);
                    
                    System.out.println("tipos de datos correptos");
                    System.out.println("quitamos "+lista.remove(i-2));
                    System.out.println("nueva forma");
                    for (int j = 0; j < lista.size(); j++) 
                    {
                        System.out.print(lista.get(j)+" ");
                    }
                    System.out.println("");
                    System.out.println("\ntambien "+lista.remove(i-2));
                    
                    tmp = (Propiedades) lista.get(i-2);
                    tmp.setTipoDato(tipoDato);
                    tmp.setLexema(tipoDato);
                    
                    System.out.println("nueva forma");
                    for (int j = 0; j < lista.size(); j++) 
                    {
                        System.out.print(lista.get(j)+" ");
                    }
                    System.out.println("");
                    lista.set(i -2, tmp);
                    
                    System.out.println("valor cambiado "+lista.get(i-2));
                    i = i-2;
                }else
                {
//                    System.out.println("errro en tipo de dato = "+tipoDato);
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Metodo para determinar que tipo de dato resulta de la una expresion entre dos elementos
     * 
     * @param dato1 primer tipo de dato
     * @param dato2 segundo tipo de dato
     * @param operador es el tipo de operacion que se realiza
     * @return retorna el tipo de dato que resulto
     */
    public Object tipo(String dato1, String dato2, String operador) 
    {
        Object tdato = "ERROR";
        Object tablaSemantico[][]= new Object[0][0]; 
        
        if(operador.equals("+") | operador.equals("-") |operador.equals("*") |operador.equals("/") )
        {
             tablaSemantico= new Object[][]
             {
                 {"","ENTERO","FLOTANTE","BOOLEANO","CADENA","DECIMAL"},
                 {"ENTERO","ENTERO","FLOTANTE","ERROR","ERROR","DECIMAL"},
                 {"FLOTANTE","FLOTANTE","FLOTANTE","ERROR","ERROR","DECIMAL"},
                 {"BOOLEANO","ERROR","ERROR","ERROR","ERROR","ERROR"},
                 {"CADENA","ERROR","ERROR","ERROR","ERROR","ERROR"},
                 {"DECIMAL","DECIMAL","DECIMAL","ERROR","ERROR","DECIMAL"},
             };
         }
         if(operador.equals("<") | operador.equals(">")| operador.equals(">=")| operador.equals("<="))
         {
             tablaSemantico= new Object[][]
             {
                 {"","ENTERO","FLOTANTE","BOOLEANO","CADENA","DECIMAL"},
                 {"ENTERO","BOOLEANO","BOOLEANO","ERROR","ERROR","BOOLEANO"},
                 {"FLOTANTE","BOOLEANO","BOOLEANO","ERROR","ERROR","BOOLEANO"},
                 {"BOOLEANO","ERROR","ERROR","ERROR","ERROR","ERROR"},
                 {"CADENA","ERROR","ERROR","ERROR","ERROR","ERROR"},
                 {"DECIMAL","BOOLEANO","LOGIOCO","ERROR","ERROR","BOOLEANO"},
             };
         }
         if(operador.equals("!=") || operador.equals("=="))
         {
            tablaSemantico= new Object[][]
            {
                {"","ENTERO","FLOTANTE","BOOLEANO","CADENA","DECIMAL"},
                {"ENTERO","BOOLEANO","BOOLEANO","ERROR","ERROR","BOOLEANO"},
                {"FLOTANTE","BOOLEANO","BOOLEANO","ERROR","ERROR","BOOLEANO"},
                {"BOOLEANO","ERROR","ERROR","BOOLEANO","ERROR","ERROR"},
                {"CADENA","ERROR","ERROR","ERROR","BOOLEANO","ERROR"},
                {"DECIMAL","BOOLEANO","BOOLEANO","ERROR","ERROR","BOOLEANO"}
            };
        }
        if(operador.equals("OR") | operador.equals("AND"))
        {
            tablaSemantico= new Object[][]
            {
                {"","ENTERO","FLOTANTE","BOOLEANO","CADENA","DECIMAL"},
                {"ENTERO","ERROR","ERROR","ERROR","ERROR","ERROR"},
                {"FLOTANTE","ERROR","ERROR","ERROR","ERROR","ERROR"},
                {"BOOLEANO","ERROR","ERROR","BOOLEANO","ERROR","ERROR"},
                {"CADENA","ERROR","ERROR","ERROR","ERROR","ERROR"},
                {"DECIMAL","ERROR","ERROR","ERROR","ERROR","ERROR","ERROR"}
            };
        }
        if(operador.equals("="))
        {
            tablaSemantico= new Object[][]
            {
                {"VACIO","ENTERO","FLOTANTE","BOOLEANO","CADENA","DECIMAL"},
                {"ENTERO","ENTERO","ERROR","ERROR","ERROR","ERROR"},
                {"FLOTANTE","ERROR","FLOTANTE","ERROR","ERROR","ERROR"},
                {"BOOLEANO","ERROR","ERROR","BOOLEANO","ERROR","ERROR"},
                {"CADENA","ERROR","ERROR","ERROR","CADENA","ERROR"},
                {"DECIMAL","ERROR","ERROR","ERROR","CADENA","DECIMAL"}
            };
        }
        
        for (int i = 0; i < tablaSemantico.length; i++) 
        {
            if (tablaSemantico[i][0].equals(dato1)) 
            {
                for (int j = 0; j < tablaSemantico[i].length; j++) 
                {
                    if (tablaSemantico[0][j].equals(dato2)) 
                    {
                        tdato = tablaSemantico[i][j];
                    }
                }
            }
        }
        return tdato;
    }
    
    /**
     * Metodo que llena una lista a partir de un arreglo de objetos
     * 
     * @param arreglo es el arreglo que convertira en una lista
     * @return retorna la lista
     */
    public List<Object> getLista(Object arreglo[])
    {
        List<Object> nvaLista = new ArrayList();
        for (int i = 0; i < arreglo.length; i++) 
        {
            nvaLista.add(arreglo[i]);
        }
        return nvaLista;
    }

    public static void main(String[] args) 
    {
//        int a = 0;
//        float b = 0;
//        Propiedades obj1 = new Propiedades("(", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj2 = new Propiedades("(", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj3 = new Propiedades("a", 1, "ENTERO", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj4 = new Propiedades("SUMA", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj5 = new Propiedades("b", 1, "DECIMAL", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj6 = new Propiedades(")", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj7 = new Propiedades("DIV", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj8 = new Propiedades("c", 1, "CADENA", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj9 = new Propiedades(")", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj10 = new Propiedades("MUL", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj11 = new Propiedades("d", 1, "CADENA", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj12 = new Propiedades(")", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        
//        Propiedades obj13 = new Propiedades("=", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj14 = new Propiedades("10", 1, "ENTERO", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj15 = new Propiedades("RESTA", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj16 = new Propiedades("10", 1, "ENTERO", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj17 = new Propiedades("SUMA", 1, "", "",new ArrayList(), 1, 1, null, "", "");
//        Propiedades obj18 = new Propiedades("1000", 1, "ENTERO", "",new ArrayList(), 1, 1, null, "", "");
////        Object arreglo[] = new Propiedades[]{obj3,obj5,obj4,obj8,obj7,obj11,obj10};
//        Object arreglo[] = new Propiedades[]{obj3,obj13,obj14,obj15,obj16,obj17,obj18};
//        
//        Evaluacion eva = new Evaluacion();
//        
//        //System.out.println(eva.Evaluacion(new Posfijo().Posfijo(arreglo)));
        
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) 
        {
            ArchivoSecuencial arch = new ArchivoSecuencial(chooser.getSelectedFile().toString());
            Evaluacion eva = new Evaluacion();
            Lexico lex = new Lexico();
            Object arreglo [] = eva.posfijo(lex.separa("10 = 10 + 10").toArray());
            
            //System.out.println("Datos en postfijo");
            for (int i = 0; i < arreglo.length; i++) 
            {
                //System.out.print(arreglo[i]+" ");
            }
            //System.out.println("la evaluacion es "+eva.evaluacion(arreglo));
        }
    }
}
