/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor;
 */
package Semantico;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tongoanda
 */
public class Propiedades 
{

    /*varibles constantes del programa*/
    public static final String GLOBAL = "global";
    public static final String LOCAL = "local";
    public static final String METODO = "metodo";
    public static final String CLASE = "clase";
    public static final String VARIABLE = "variable";
    public static final String ARREGLO = "variable";
    public static final String PARAMETRO = "parametro";
    public static final String USO = "usada";
    public static final String SIN_USO = "no usada";
    public static final List<Propiedades> SIN_VALOR = new ArrayList();
    public static final String CONDICION = "condicion";
    public static final String SIN_LEXEMA = "sin lexema";
    public static final String SIN_TIPO_DATO = "sin tipo dato";
    public static final String SIN_METODO = "sin metodo";
    public static final int SIN_NO_TOKEN = 0;
    
    private String lexema;      //lexema del elemento
    private int noToken;        //numero de token
    private String tipoDato;    //tipo de dato
    private String uso;         //uso de la variable
    private List<Propiedades> valor;       //valor de la variable
    private int noLinea;        //numero de linea de la variable
    private int columna;        //numero de columna de la variable
    private List<Propiedades> parametros;  //parametros de un metodo
    private String acceso;      //acceso local o global
    private String tipoSimbolo; //tipo de simbolo
    private String metodo;
    private int tamanio;
    
    /*
     * Constructor para asignarle valores a las variables de la clase
     **/
    public Propiedades(String lexema, int noToken, String tipoDato, String uso, List<Propiedades> valor, int noLinea, int columna, List<Propiedades> parametros, String acceso, String tipoSimbolo) 
    {
        this.lexema = lexema;
        this.noToken = noToken;
        this.tipoDato = tipoDato;
        this.uso = uso;
        this.valor = valor;
        this.noLinea = noLinea;
        this.columna = columna;
        this.parametros = parametros;
        this.acceso = acceso;
        this.tipoSimbolo = tipoSimbolo;
    }

    public Propiedades(String lexema, int noToken, String tipoDato, String uso, List<Propiedades> valor, int noLinea, int columna, List<Propiedades> parametros, String acceso, String tipoSimbolo, String metodo, int tamanio) {
        this.lexema = lexema;
        this.noToken = noToken;
        this.tipoDato = tipoDato;
        this.uso = uso;
        this.valor = valor;
        this.noLinea = noLinea;
        this.columna = columna;
        this.parametros = parametros;
        this.acceso = acceso;
        this.tipoSimbolo = tipoSimbolo;
        this.metodo = metodo;
        this.tamanio = tamanio;
    }
    
    

    /**
     * Constructor para los parametros
     */
    public Propiedades(String lexema, int noToken, String tipoDato, String uso, List<Propiedades> valor, int noLinea, int columna, List<Propiedades> parametros, String acceso, String tipoSimbolo, String metodo) 
    {
        this.lexema = lexema;
        this.noToken = noToken;
        this.tipoDato = tipoDato;
        this.uso = uso;
        this.valor = valor;
        this.noLinea = noLinea;
        this.columna = columna;
        this.parametros = parametros;
        this.acceso = acceso;
        this.tipoSimbolo = tipoSimbolo;
        this.metodo = metodo;
    }
    
    
    
    /**
     * Metodo que despliega la informacion de las Propiedades
     * @return retorna todas las Propiedades del objeto
     */
    public String desp()
    {
        String cadena = 
                "Lexema: "+lexema+"\t"
                + "No.Token: "+noToken+"\t"
                + "Tipo Dato: "+tipoDato+"\t"
                + "Uso: "+uso+"\t"
                + "Valor: ";
                
                if (valor != null) 
                {
//                    for (int i = 0; i < valor.size(); i++) 
//                    {
//                        cadena += valor.get(i).getLexema()+" ";
//                    }
                    cadena += valor+"\t";
                }else
                {
                    System.out.println("null");
                }
                     
                cadena += "Linea: "+noLinea+"\t"
                + "Columna: "+columna+"\t"
                + "Parametros: ";
                if (parametros != null) 
                {
                    for (int i = 0; i < parametros.size(); i++) 
                    {
                        cadena += parametros.get(i).getTipoDato()+" "+parametros.get(i).getLexema()+" ";
                    }
                    cadena += "\t";
                }
                cadena += "Acceso: "+acceso+"\t"
                       + "Tipo Simbolo: "+tipoSimbolo+"\t"
                        + "Metodo: "+metodo;
        return cadena;
    }

    /**
     * Sobreescrimimos el metodo toString para que al mostrar el objeto como cadena muestre el lexema
     *
     * @return retorna el nombre del lexema
     */
    @Override
    public String toString() 
    {
        return lexema;
    }
    
    /*GETERS Y SETERS*/
    public String getAcceso() {
        return acceso;
    }

    public int getColumna() {
        return columna;
    }

    public String getLexema() {
        return lexema;
    }

    public int getNoLinea() {
        return noLinea;
    }

    public int getNoToken() {
        return noToken;
    }

    public List<Propiedades> getParametros() {
        return parametros;
    }

    public String getTipoDato() {
        return tipoDato;
    }

    public String getTipoSimbolo() {
        return tipoSimbolo;
    }

    public String getUso() {
        return uso;
    }

    public List<Propiedades> getValor() {
        return valor;
    }

    public void setAcceso(String acceso) {
        this.acceso = acceso;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }

    public void setNoLinea(int noLinea) {
        this.noLinea = noLinea;
    }

    public void setNoToken(int noToken) {
        this.noToken = noToken;
    }

    public void setParametros(List<Propiedades> parametros) {
        this.parametros = parametros;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public void setTipoSimbolo(String tipoSimbolo) {
        this.tipoSimbolo = tipoSimbolo;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public void setValor(List<Propiedades> valor) {
        this.valor = valor;
    }

    public String getMetodo() {
        return metodo;
    }

    public void setMetodo(String metodo) {
        this.metodo = metodo;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }
    
}
